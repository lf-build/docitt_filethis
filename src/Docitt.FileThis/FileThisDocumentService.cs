﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    /// <summary>
    /// DocumentService
    /// </summary>
    public class FileThisDocumentService : IFileThisDocumentService
    {
        /// <summary>
        /// DocumentService
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="tenantTime">tenantTime</param>
        /// <param name="configuration">configuration</param>
        public FileThisDocumentService(ILogger logger,
            IDocumentRepository documentRepository,
            IDocumentManagerService documentManagerClient,
            ITenantTime tenantTime,
            IConfiguration configuration)
        {
            this.Configuration = configuration;
            this.TenantTime = tenantTime;
            this.DocumentRepository = documentRepository;
            this.DocumentManagerService = documentManagerClient;
            if (Configuration == null)
                throw new ArgumentException("Filethis configuration cannot be found, please check");
        }

        /// <summary>
        /// Get Tenant Time 
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets DocumentRepository
        /// </summary>
        private IDocumentRepository DocumentRepository { get; }

        /// <summary>
        /// Gets DocumentManagerService
        /// </summary>
        private IDocumentManagerService DocumentManagerService { get; }

        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// GetDocumentsByConnectionId
        /// </summary>
        /// <param name="connectionId">connectionId</param>
        /// <returns>document list</returns>
        public async Task<IList<IDocument>> GetDocumentsByConnectionId(string connectionId)
        {
            var documentList = await this.DocumentRepository.GetDocuments(connectionId, true);
            return documentList;
        }

        /// <summary>
        /// DeleteDocument
        /// </summary>
        /// <param name="documentId">documentId</param>
        /// <returns></returns>
        public async Task<bool> DeleteDocument(string documentId)
        {
            var documentData = await this.DocumentRepository.GetDocumentById(documentId);

            if (documentData != null)
            {
                documentData.IsDeleted = true;
                documentData.FileThisModifiedDate = TenantTime.Now.DateTime;
                return await this.DocumentRepository.UpdateDocument(documentData);
            }
            
            return false;
        }

        /// <summary>
        /// DownloadDocument
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="documentId">documentId</param>
        /// <returns></returns>
        public async Task<Tuple<string, Stream>> DownloadDocument(string applicantId, string documentId)
        {
            var documentList = await this.DocumentManagerService.GetAll("filethis", applicantId);
            var documentManagerId = string.Empty;
            var documentName = string.Empty;
            foreach (var item in documentList)
            {
                var document = JsonConvert.DeserializeObject<DocumentMetaData>(item.Metadata.ToString());

                if(document.DocumentId == documentId)
                {
                    documentManagerId = item.Id;
                    documentName = item.FileName;
                    
                    break;
                }
            }
           
            var downloadFile = await this.DocumentManagerService.Download("filethis", applicantId, documentManagerId);

            return new Tuple<string, Stream>(documentName, downloadFile);
        }

        /// <summary>
        /// UpdateDocumentAsync
        /// </summary>
        /// <param name="connectionId">connectionId</param>
        /// <returns></returns>
        public async Task<IList<IDocument>> UpdateDocumentAsync(string connectionId)
        {
            var documentList = await this.DocumentRepository.GetDocuments(connectionId, false);

            foreach (var documentItem in documentList)
            {
                documentItem.IsDeleted = false;
                documentItem.FileThisModifiedDate = TenantTime.Now;
                var ack = await this.DocumentRepository.UpdateDocument(documentItem);
            }

            return documentList;
        }
    }
}
