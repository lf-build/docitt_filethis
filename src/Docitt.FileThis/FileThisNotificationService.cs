﻿using Docitt.FileThis.Abstractions;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    /// <summary>
    /// ChangeNotificationService
    /// </summary>
    public class FileThisNotificationService : IFileThisNotificationService
    {
        /// <summary>
        /// ChangeNotificationService
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="tenantTime">tenantTime</param>
        /// <param name="configuration">configuration</param>
        public FileThisNotificationService(ILogger logger,
            IChangeRepository changeRepository,
            IDocumentRepository documentRepository,
            IFileThisClient fileThisClient,
            ITenantTime tenantTime,
            IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentException("Filethis configuration cannot be found, please check");
            this.Configuration = configuration;
            this.TenantTime = tenantTime;
            this.ChangeRepository = changeRepository;
            this.FileThisClient = fileThisClient;
            this.DocumentRepository = documentRepository;
        }

        /// <summary>
        /// Get Tenant Time 
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets ChangeRepository
        /// </summary>
        private IChangeRepository ChangeRepository { get; }

        /// <summary>
        /// Gets DocumentRepository
        /// </summary>
        private IDocumentRepository DocumentRepository { get; }

        /// <summary>
        /// Gets FileThisClient
        /// </summary>
        private IFileThisClient FileThisClient { get; }

        /// <summary>
        /// GetChanges
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns></returns>
        public async Task<IChange> GetChange(string accountId, string connectionId)
        {
            var state = await this.ChangeRepository.GetLatestChange(accountId, connectionId);

            if (state != null)
            {
                var connectionCheck = await this.FileThisClient.GetConnectionAsync(accountId, connectionId);
                if (!string.IsNullOrWhiteSpace(connectionCheck.Id))
                {
                    state.Resource.State = connectionCheck.State;
                }
            }
            else
            {
                // Todo: Discuss with FileThis support team to understand if change notifications are not send, should we pull manually. 
            
            }
            return state;
        }

        /// <summary>
        /// InsertChangeNotifications
        /// </summary>
        /// <param name="changes">changes</param>
        /// <returns></returns>
        public async Task InsertChangeNotifications(FileThisDataHook changeNotification)
        {
            if (changeNotification.Changes.Any())
            {
                var changes = this.GetChangeList(changeNotification);
                await this.ChangeRepository.AddChanges(changes);
            }
        }

        /// <summary>
        /// InsertDocuments
        /// </summary>
        /// <param name="metaData">metaData</param>
        /// <returns></returns>
        public async Task InsertDocuments(string metaData)
        {
            var document = JsonConvert.DeserializeObject<Document>(metaData);

            if (document == null)
            {
                throw new InvalidArgumentException("metadata is not parse properly");
            }

            ///Insert into DB
            document.FileThisCreatedDate = TenantTime.Now;
            var ack = await this.DocumentRepository.AddDocument(document);
        }

        /// <summary>
        /// GetChangeList
        /// </summary>
        /// <param name="changeNotification">changeNotification</param>
        /// <returns></returns>
        private IList<IChange> GetChangeList(FileThisDataHook changeNotification)
        {
            var changeList = new List<IChange>();
            foreach (var changeItem in changeNotification.Changes)
            {
                IChange change = new Change();
                var resource = new Resource();
                var resourceStringArray = changeItem.Resource.Split('/');

                for (int resourceItem = 0; resourceItem < resourceStringArray.Count(); resourceItem++)
                {
                    switch (resourceStringArray[resourceItem])
                    {
                        case "accounts":
                            resource.AccountId = resourceStringArray[resourceItem + 1].ToString();
                            continue;

                        case "connections":
                            resource.ConnectionId = resourceStringArray[resourceItem + 1].ToString();
                            continue;

                        case "interactions":
                            resource.InteractionId = resourceStringArray[resourceItem + 1].ToString();
                            continue;

                        case "state":
                            resource.State = resourceStringArray[resourceItem + 1].ToString();
                            continue;
                    }
                }

                change.Resource = resource;
                change.ChangeId = changeItem.Id.ToString();
                change.PartnerId = this.Configuration.PartnerId.ToString();
                //change.SubscriberId = "";
                //change.TenantId = "my-tenant";
                change.Type = changeItem.Type;
                change.SubscriptionId = changeItem.SubscriptionId;
                changeList.Add(change);
            }

            return changeList;
        }
    }
}
