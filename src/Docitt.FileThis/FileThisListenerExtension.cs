﻿using Docitt.FileThis.Abstractions;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.FileThis
{
    /// <summary>
    /// FileThisListenerExtensions 
    /// </summary>
    public static class FileThisListenerExtensions
    {
        public static void UseFileThisListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IFileThisListener>().Start();
        }
    }
}
