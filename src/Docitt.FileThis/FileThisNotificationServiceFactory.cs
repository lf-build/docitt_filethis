﻿using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace Docitt.FileThis
{
    public class FileThisNotificationServiceFactory : IFileThisNotificationServiceFactory
    {
        public FileThisNotificationServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFileThisNotificationService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var fileThisClientFactory = Provider.GetService<IFileThisClientFactory>();
            var filethisClient = fileThisClientFactory.Create(reader, handler, logger);

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var repositoryFactory = Provider.GetService<IRepositoryFactory>();
            var changeRepository = repositoryFactory.CreateChangeRepository(reader);
            var documentRepository = repositoryFactory.CreateDocumentRepository(reader);

            return new FileThisNotificationService(
                logger,
                changeRepository,
                documentRepository,
                filethisClient,
                tenantTime,
                configuration);
        }
    }
}
