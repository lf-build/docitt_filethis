﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;

namespace Docitt.FileThis
{
    /// <summary>
    /// PartnerService
    /// </summary>
    public class FileThisPartnerService : IFileThisPartnerService
    {
        /// <summary>
        /// FileThis Patner Service
        /// </summary>
        /// <param name="filethisClientService">FileThis Client</param>
        /// <param name="configuration">FileThis Configuation</param>
        /// <param name="logger">Logger</param>
        public FileThisPartnerService(
            IFileThisClient filethisClientService,
            IConfiguration configuration,
            ILogger logger)
        {
            Configuration = configuration ?? throw new ArgumentException("Filethis configuration cannot be found, please check");
            FilethisClientService = filethisClientService;
            Log = logger;
        }

        /// <summary>
        /// Logger Property
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private IFileThisClient FilethisClientService { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// GetPartners
        /// </summary>
        /// <returns>response</returns>
        public async Task<IList<FilethisPartnerResponse>> GetPartnersAsync()
        {
            Log.Debug("Inside GetPartnersAsync...");
            return await FilethisClientService.GetPartnersFilethisAsync();
        }

        /// <summary>
        /// GetPartners
        /// </summary>
        /// <returns>response</returns>
        public async Task<FilethisPartnerResponse> GetPartnersByIdAsync(string partnerID)
        {
            Log.Debug("Inside GetPartnersAsync...");
            return await FilethisClientService.GetPartnerByPartnerIdAsync(partnerID);
        }
    }
}
