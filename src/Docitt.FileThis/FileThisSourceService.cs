﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    /// <summary>
    /// PartnerService
    /// </summary>
    public class FileThisSourceService : IFileThisSourceService
    {
        /// <summary>
        /// SourceService
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="tenantTime">tenantTime</param>
        /// <param name="configuration">configuration</param>
        public FileThisSourceService(ILogger logger,
            ITenantTime tenantTime,
            IConfiguration configuration,
            ISourceRepository repository)
        {
            this.Configuration = configuration;
            this.TenantTime = tenantTime;
            this.Repository = repository;

            if (Configuration == null)
                throw new ArgumentException("Filethis configuration cannot be found, please check");
        }

        /// <summary>
        /// Get Tenant Time 
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private ISourceRepository Repository { get; }

        /// <summary>
        /// GetSourcesAsync
        /// </summary>
        /// <returns>response</returns>
        public async Task<IList<ISource>> GetSourcesAsync()
        {
            return await this.Repository.GetSourcesAsync();
        }

        /// <summary>
        /// GetTaxReturnSourcesAsync
        /// </summary>
        /// <returns>response</returns>
        public async Task<IList<IResponseSource>> SearchSourceAsync(string categoryType, string keyword)
        {
            IList<IResponseSource> sourceResponse = new List<IResponseSource>();
            var sourcesResult = await this.Repository.GetSourcesAsync(categoryType, keyword);

            foreach (var sourceItem in sourcesResult)
            {
                IResponseSource source = new ResponseSource();
                source.SourceId = sourceItem.SourceId;
                source.SourceName = sourceItem.SourceName;
                source.Logo = sourceItem.LogoUrl;

                sourceResponse.Add(source);
            }

            return sourceResponse;
        }

        /// <summary>
        /// GetSourceById
        /// </summary>
        /// <returns>response</returns>
        public async Task<IResponseSourceDetail> GetSourceByIdAsync(string sourceId)
        {
            var source = await this.Repository.GetSourceByIdAysnc(sourceId);

            if (source != null)
            {
                IResponseSourceDetail sourceData = new ResponseSourceDetail();
                sourceData.SourceId = source.SourceId;
                sourceData.SourceName = source.SourceName;
                sourceData.Logo = source.LogoUrl;
                sourceData.Credential = this.CreatingCredentialModel();
                return sourceData;
            }
            return null;
        }

        /// <summary>
        /// CheckPlaidInstitutionIdExist
        /// </summary>
        /// <param name="plaidInstitutionId">plaidInstitutionId</param>
        /// <returns></returns>
        public async Task<string> CheckPlaidInstitutionIdExist(string plaidInstitutionId)
        {
            if (!string.IsNullOrWhiteSpace(plaidInstitutionId))
            {
                return await this.Repository.GetFileThisSourceIdAysnc(plaidInstitutionId);
            }

            return string.Empty;
        }

        /// <summary>
        /// CreatingCredentialModel
        /// </summary>
        /// <returns></returns>
        private IList<FileThisCredential> CreatingCredentialModel()
        {
            var credentialList = new List<FileThisCredential>();
            var credentialUsername = new FileThisCredential
            {
                Name = "username",
                Label = "Username",
                Type = "text"
            };
            var credentialPassword = new FileThisCredential
            {
                Name = "password",
                Label = "Password",
                Type = "password"
            };

            credentialList.Add(credentialUsername);
            credentialList.Add(credentialPassword);

            return credentialList;
        }

    }
}
