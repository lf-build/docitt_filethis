﻿using Docitt.FileThis.Abstractions;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Listener;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace Docitt.FileThis
{
    public class FileThisListener : ListenerBase, IFileThisListener
    {
        public FileThisListener
           (
               IConfigurationServiceFactory configurationFactory,
               ITokenHandler tokenHandler,
               IEventHubClientFactory eventHubFactory,
               ILoggerFactory loggerFactory,
               ITenantServiceFactory tenantServiceFactory,
               IFileThisNotificationServiceFactory fileThisNotificationServiceFactory
           ) : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {

            EventHubFactory = eventHubFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            FileThisNotificationServiceFactory = fileThisNotificationServiceFactory;
            Logger = loggerFactory.Create(NullLogContext.Instance);
        }

        private IFileThisNotificationServiceFactory FileThisNotificationServiceFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILogger Logger { get; }        

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            try
            {
                var token = TokenHandler.Issue(tenant, "PlaidIssuer");
                var reader = new StaticTokenReader(token.Value);
                var eventhub = EventHubFactory.Create(reader);
                var changeNotificationService = FileThisNotificationServiceFactory.Create(reader, TokenHandler, Logger);

                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                configurationService.ClearCache(Settings.ServiceName);
                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                    return null;
                }
                else
                {
                    logger.Info($"#{configuration.Events.Count()} entity(ies) found from configuration: {Settings.ServiceName}");

                    var uniqueEvents = configuration.Events                      
                        .Distinct()
                        .ToList();

                    uniqueEvents.ForEach(eventConfig =>
                    {
                        eventhub.On(eventConfig.Name, UpdateStatus(eventConfig, logger, changeNotificationService));
                        logger.Info($"It was made subscription to EventHub with the Event: #{eventConfig.Name} for tenant {tenant}");
                    });

                    return uniqueEvents.Select(x=>x.Name).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Unable to subscribe event for ${tenant}", ex);
                return new List<string>();
            }
        }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();

            return configuration?.Events.Select(x=>x.Name).Distinct().ToList();
        }

        private static Action<EventInfo> UpdateStatus(
            EventMapping eventConfiguration, 
            ILogger logger, 
            IFileThisNotificationService fileThisNotificationService)
        {
            return @event =>
            {
                try
                {
                    if (eventConfiguration.Name.Equals("DocumentMetaData"))
                    {
                        var data = eventConfiguration.Data.FormatWith(@event);
                        logger.Debug($"{data}");

                        logger.Debug("Calling InsertDocument");
                        fileThisNotificationService.InsertDocuments(data).Wait();
                    }
                    else if(eventConfiguration.Name.Equals("INBOUND_HOOK_INVOKED"))
                    {
                        var eventName = eventConfiguration.EventName.FormatWith(@event);
                        logger.Debug($"eventName: {eventName} received");

                        if (eventName == "filethisnotification")
                        {
                            var data = eventConfiguration.Data.FormatWith(@event);
                            logger.Debug($"{data}");
                            var changesObject = JsonConvert.DeserializeObject<FileThisDataHook>(data);
                            logger.Debug("Calling InsertChanges");
                            fileThisNotificationService.InsertChangeNotifications(changesObject).Wait();
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
            };
        }
    }
}
