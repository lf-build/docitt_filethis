﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Security.Tokens;

namespace Docitt.FileThis
{
    /// <summary>
    /// PartnerService
    /// </summary>
    public class FileThisConnectionService : IFileThisConnectionService
    {
        #region constructor and Declarations

        /// <summary>
        /// PartnerService
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="filethisClientService">filethisClientService</param>
        /// <param name="tenantTime">tenantTime</param>
        /// <param name="configuration">configuration</param>
        public FileThisConnectionService(ILogger logger,
            IFileThisClient filethisClientService,
            IAccountRepository accountRepository,
            ISourceRepository sourceRepository,
            IDocumentRepository documentRepository,
            ITenantTime tenantTime,
            IConfiguration configuration,
            ITokenHandler tokenHandler,
            ITokenReader tokenReader)
        {
            if (configuration == null)
                throw new ArgumentException("Filethis configuration cannot be found, please check");

            this.Configuration = configuration;
            this.TenantTime = tenantTime;
            this.FilethisClientService = filethisClientService;
            this.AccountRepository = accountRepository;
            this.SourceRepository = sourceRepository;
            this.DocumentRepository = documentRepository;
            this.Logger = logger;
            this.TokenHandler = tokenHandler;
            this.TokenReader = tokenReader;
        }

        /// <summary>
        /// Get Tenant Time 
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private IFileThisClient FilethisClientService { get; }

        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// AccountRepository
        /// </summary>
        private IAccountRepository AccountRepository { get; set; }

        /// <summary>
        /// Gets SourceRepository
        /// </summary>
        private ISourceRepository SourceRepository { get; set; }

        /// <summary>
        /// Gets filethisResponse
        /// </summary>
        private IDocumentRepository DocumentRepository { get; set; }

        /// <summary>
        /// Logger
        /// </summary>
        /// <value></value>
        private ILogger Logger {get; set;}

        
        /// <summary>
        /// TokenHandler
        /// </summary>
        /// <value></value>
        private ITokenHandler TokenHandler {get; set;}

        /// <summary>
        /// TokenHandler
        /// </summary>
        /// <value></value>
        private ITokenReader TokenReader {get; set;}

        #endregion

        #region Methods

        public async Task<IFilethisConnectionResponse> GetStatus(IFileThisExistAccount request)
        {
            var connectionResponse = await this.FilethisClientService.GetConnectionAsync(request.FileThisAccountId, request.FileThisConnectionId);
            return connectionResponse;
        }

        public Task<IAccount> GetConnection(string connectionId)
        {
            return this.AccountRepository.GetConnection(connectionId);
        }

        /// <summary>
        /// ConnectionWorkflowAsync
        /// </summary>
        /// <returns>response</returns>
        public async Task<IResponseConnection> ConnectionWorkflowAsync(IRequestConnection request)
        {
            Logger.Debug($"ConnectionWorkflowAsync applicationNumber {request.ApplicationNumber} applicant {request.ApplicantId}");

            if (string.IsNullOrWhiteSpace(request.Username) && string.IsNullOrWhiteSpace(request.Password))
            {
                throw new ArgumentException("Credentials are required");
            }

            Logger.Debug($"GenericResponseConnection {request.ApplicationNumber} applicant {request.ApplicantId}");
            var result = await this.GenericResponseConnection(request);
            result.ApplicantId = request.ApplicantId;

            //// AccountId flow
            //// Repository call if exist or not
            Logger.Debug($"Create account for {request.ApplicationNumber} applicant {request.ApplicantId}");
            var existAccountResult = await this.CreateAccountId(request.ApplicationNumber, request.ApplicantId, request.SourceId);
            result.AccountId = existAccountResult.FileThisAccountId;
            result.ConnectionId = existAccountResult.FileThisConnectionId;

            //// Token setup
            Logger.Debug($"Generate Token for {request.ApplicationNumber} applicant {request.ApplicantId} connectionId {result.ConnectionId}");
            await this.TokenSetUp(result.AccountId);
            //var token = await this.GenerateFileThisToken(result.AccountId);

            //// create a connection
            Logger.Debug($"Create new connection for {request.ApplicationNumber} applicant {request.ApplicantId}");
            var newConnectionResponse = await this.FilethisClientService.CreateNewConnectionAsync(request, result.AccountId);

            if (string.IsNullOrWhiteSpace(newConnectionResponse.Id))
            {
                Logger.Warn($"Error in create new connection for {request.ApplicationNumber} applicant {request.ApplicantId}");
                result.IsError = true;
                result.Error = new ResponseError
                {
                    ErrorMessage = newConnectionResponse.Message,
                    Status = newConnectionResponse.Status
                };

                return result;
            }

            result.ConnectionId = newConnectionResponse.Id;
            Logger.Debug($"Create new connection for {request.ApplicationNumber} applicant {request.ApplicantId} new connectionid {result.ConnectionId}");

            var connectionResponse = await this.FilethisClientService.GetConnectionAsync(result.AccountId, result.ConnectionId);
            var state = this.ParseEnum(connectionResponse.State);
            if (state == ConnectionState.Question)
            {
                //// create an interactions
                result.IsMfa = true;
                result.Mfa = await this.FilethisClientService.GetInteractionAsync(result.AccountId, result.ConnectionId);

                if (string.IsNullOrWhiteSpace(result.Mfa.Id))
                {
                    throw new ArgumentException("Waiting for MFA");
                }
                result.InteractionId = result.Mfa.Id;
            }

            //DB insert
            Logger.Debug($"InsertUpdateAccount for application {request.ApplicationNumber} applicant {request.ApplicantId}");
            var ack = await this.InsertUpdateAccount(request, result, connectionResponse, existAccountResult.ApplicantExists);

            return result;
        }

        public async Task<IResponseConnection> CreateInteractionAsync(IRequestInteraction request)
        {
            if (string.IsNullOrWhiteSpace(request.AccountId) && string.IsNullOrWhiteSpace(request.ConnectionId)
                && string.IsNullOrWhiteSpace(request.ApplicantId))
            {
                throw new ArgumentException("ApplicantId, AccountId and ConnectionId are required");
            }

            var result = await this.GenericResponseConnection(request);
            result.ApplicantId = request.ApplicantId;
            result.AccountId = request.AccountId;
            result.ConnectionId = request.ConnectionId;

            //// create an interactions
            result.IsMfa = true;
            result.Mfa = await this.FilethisClientService.GetInteractionAsync(result.AccountId, result.ConnectionId);

            if (string.IsNullOrWhiteSpace(result.Mfa.Id))
            {
                throw new ArgumentException("Waiting for MFA");
            }

            result.InteractionId = result.Mfa.Id;
            return result;
        }

        public async Task<bool> InteractionResponseAsync(IRequestInteraction request)
        {
            var response = await this.FilethisClientService.InteractionResponseAsync(request);
            return response;
        }

        public async Task<IFilethisConnectionResponse> ConnectionCheck(string applicantId, string accountId, string connectionId)
        {
            var connectionResponse = await this.FilethisClientService.GetConnectionAsync(accountId, connectionId);

            //// update account connection object
            var connectionData = await this.ConnectionMapper(connectionResponse, string.Empty);
            var flag = await this.AccountRepository.UpdateConnection(applicantId, accountId, connectionData);
            return connectionResponse;
        }


        public async Task<IAccount> GetApplicantDetail(string applicationNumber, string applicantId)
        {
            var account = await this.AccountRepository.GetAccountByApplicant(applicationNumber, applicantId);

            return account;
        }

        public async Task<string> GetConnectionFetch(string accountId, string connectionId)
        {
            var response = await this.FilethisClientService.FetchConnectionAsync(accountId, connectionId);
            return response.Message;
        }

        /// <summary>
        /// DeleteConnectionAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns>True or False</returns>
        public async Task<bool> DeleteConnectionAsync(string accountId, string connectionId, string applicationNumber)
        {
            var filethisResponse = await this.FilethisClientService.DeleteConnectionAsync(accountId, connectionId);

            if (filethisResponse)
            {
                //DB remove connection node
                var response = await this.AccountRepository.DeleteConnection(accountId, connectionId);

                if (response)
                {
                    var ack = await this.DocumentRepository.UpdateDocuments(connectionId, TenantTime.Now);
                }
            }
            return filethisResponse;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// TokenSetUp
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns></returns>
        private async Task TokenSetUp(string accountId)
        {
            var accountResponse = await this.FilethisClientService.GetAccountAsync(accountId);

            if (accountResponse == null)
            {
                //// create a token for the connection
                var newTokenResponse = await this.FilethisClientService.CreateTokenAsync(accountId);

                if (string.IsNullOrWhiteSpace(newTokenResponse.Token))
                {
                    throw new ArgumentException("Filethis services are not responded");
                }
            }
        }

        private async Task<string> GenerateFileThisToken(string accountId)
        {
            var newTokenResponse = await this.FilethisClientService.CreateTokenAsync(accountId);

            if (newTokenResponse == null || string.IsNullOrWhiteSpace(newTokenResponse.Token))
            {
                throw new ArgumentException("Failed to generate filethis token");
            }

            return newTokenResponse.Token;
        }

        private async Task<bool> GetConnectionDetailandUpdationAsync(string applicantId, string accountId, string connectionId)
        {
            var connectionResponse = await this.FilethisClientService.GetConnectionAsync(accountId, connectionId);
            var connectionData = await this.ConnectionMapper(connectionResponse, string.Empty);
            return await this.AccountRepository.UpdateConnection(applicantId, accountId, connectionData);
        }

        /// <summary>
        /// CreateAccountId
        /// </summary>
        /// <param name="applicationNumber">applicant number</param>
        /// <param name="applicantId">applicantId</param>
        /// <returns>accountId</returns>
        private async Task<IFileThisExistAccount> CreateAccountId(string applicationNumber, string applicantId, string sourceId)
        {
            IFileThisExistAccount existAccount = new FileThisExistAccount();
            existAccount.ApplicantExists = false;

            var accountData = await this.AccountRepository.GetAccountByApplicant(applicationNumber, applicantId);
            if (accountData == null)
            {
                Logger.Debug($"Creating new Account application {applicationNumber} applicant {applicantId}");
                //// create a new Account Id for the connection
                string fileThisApplicantId = await this.GeneratePartnerCustomerId(applicationNumber, applicantId);
                var newAccountResponse = await this.FilethisClientService.CreateNewAccountAsync(fileThisApplicantId);

                if (string.IsNullOrWhiteSpace(newAccountResponse.Id))
                {
                    throw new ArgumentException(newAccountResponse.Message);
                }

                existAccount.FileThisAccountId = newAccountResponse.Id;
                existAccount.FileThisConnectionId = null;
            }
            else
            {
                Logger.Debug($"Account already exists for application {applicationNumber} applicant {applicantId}");
                existAccount.FileThisAccountId = accountData.FileThisAccountId;
                accountData.Connection.ForEach(connection =>
                                                {
                                                    if (connection.SourceId == sourceId)
                                                        existAccount.FileThisConnectionId = connection.ConnectionId;
                                                });
                existAccount.ApplicantExists = true;
            }

            return existAccount;
        }

        private ConnectionState ParseEnum(string value)
        {
            return (ConnectionState)Enum.Parse(typeof(ConnectionState), value, ignoreCase: true);
        }

        private async Task<bool> InsertUpdateAccount(IRequestConnection request, IResponseConnection result,
                                IFilethisConnectionResponse connectionResponse, bool applicantExists)
        {
            if (applicantExists)
            {
                if (connectionResponse != null)
                {
                    return await this.AccountRepository.AddConnection(
                                request.ApplicationNumber,
                                request.ApplicantId, 
                                result.AccountId,
                                await this.ConnectionMapper(connectionResponse, result.InteractionId));
                }
                return false;
            }
            else
            {
                IAccount account = new Account();
                account.ApplicantId = request.ApplicantId;
                account.CreatedDate = this.TenantTime.Now.DateTime;
                account.FileThisAccountId = result.AccountId;
                account.PartnerCustomerId = await this.GeneratePartnerCustomerId(request.ApplicationNumber,request.ApplicantId);
                account.IsActive = true;
                account.PartnerId = this.Configuration.PartnerId.ToString();
                account.Connection = new List<IConnection>();
                if (connectionResponse != null)
                {
                    account.Connection.Add(await this.ConnectionMapper(connectionResponse, result.InteractionId));
                }
                account.ApplicationNumber = request.ApplicationNumber;

                return await this.AccountRepository.AddAccount(account);
            }
        }

        private async Task<string> GeneratePartnerCustomerId(string applicationNumber, string applicantId)
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            string delimiter = ":";
            string unqiueApplicantId = token.Tenant + delimiter + applicationNumber + delimiter + applicantId;
            return await Task.Run(() => unqiueApplicantId);
        }

        private async Task<Connection> ConnectionMapper(IFilethisConnectionResponse connectionResponse, string interactionId)
        {
            var connection = new Connection();
            if (!string.IsNullOrWhiteSpace(connectionResponse.SourceId))
            {
                var sourceData = await this.SourceRepository.GetSourceByIdAysnc(connectionResponse.SourceId);
                if (sourceData != null)
                {
                    connection.SourceLogoUrl = sourceData.LogoUrl;
                    connection.Tags = sourceData.Tags;
                }
            }

            connection.AttemptDate = connectionResponse.AttemptDate;
            connection.CheckedDate = connectionResponse.CheckedDate;
            connection.ConnectionId = connectionResponse.Id;
            connection.CreatedDate = connectionResponse.CreatedDate;
            connection.DocumentCount = connectionResponse.DocumentCount;
            connection.Enabled = connectionResponse.Enabled;
            connection.FetchAll = connectionResponse.FetchAll;
            connection.HistoricalPeriod = Convert.ToInt32(connectionResponse.HistoricalPeriod);
            connection.Info = connectionResponse.Info;
            connection.InteractionId = interactionId;
            connection.KickOffDate = connectionResponse.KickoffDate;
            connection.Period = connectionResponse.Period;
            connection.SourceId = connectionResponse.SourceId;
            connection.SourceName = connectionResponse.Name;
            connection.State = connectionResponse.State;
            connection.SuccessDate = connectionResponse.SuccessDate;
            connection.Tries = connectionResponse.Tries;
            connection.Username = connectionResponse.Username;
            connection.Validation = connectionResponse.Validation;

            return connection;
        }

        private string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }
        private string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        /// <summary>
        /// SourceInformation
        /// </summary>
        /// <param name="sourceId">sourceId</param>
        /// <returns>Source Information</returns>
        private async Task<ISource> SourceInformation(string sourceId)
        {
            if (!string.IsNullOrWhiteSpace(sourceId))
            {
                return await this.SourceRepository.GetSourceByIdAysnc(sourceId);
            }

            return null;
        }

        /// <summary>
        /// GenericResponseConnection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private async Task<IResponseConnection> GenericResponseConnection(dynamic request)
        {
            IResponseConnection result = new ResponseConnection();
            result.SourceId = request.SourceId.ToString();

            var sourceData = await this.SourceInformation(result.SourceId);
            if (sourceData != null)
            {
                result.SourceName = sourceData.SourceName;
                result.Logo = sourceData.LogoUrl;
            }
            return result;
        }
        #endregion
    }
}
