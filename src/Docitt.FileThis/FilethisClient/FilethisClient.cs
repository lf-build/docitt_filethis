﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using RestSharp;

namespace Docitt.FileThis
{
    /// <summary>
    /// FilethisClient
    /// </summary>
    public class FileThisClient : IFileThisClient
    {
        #region Constructor and Declarations
        /// <summary>
        /// FilethisClient
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="configuration">configuration</param>
        public FileThisClient(ILogger logger, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentException("Filethis configuration cannot be found, please check");

            Logger = logger;
            this.Configuration = configuration;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            this.Client = new RestClient(this.Configuration.Url + this.Configuration.Version);

            this.EncodedAuthenticationString = Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}:{1}",
                                                                        this.Configuration.Key, this.Configuration.Secret)));

            
        }

        /// <summary>
        /// Token
        /// </summary>
        /// <value></value>
        public string FileThisToken {get; set;}

        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// Gets Client
        /// </summary>
        private IRestClient Client { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets EncodedAuthenticationString
        /// </summary>
        private string EncodedAuthenticationString { get; }

        /// <summary>
        /// Gets SourceRepository
        /// </summary>
        private ISourceRepository SourceRepository { get; set; }

        /// <summary>
        /// Logger
        /// </summary>
        /// <value></value>
        private ILogger Logger {get; set;}

        #endregion

        #region Connections

        //// POST Create a source connection

        /// <summary>
        /// CreateNewConnectionAsync
        /// </summary>
        /// <param name="requestConnection">requestConnection</param>
        /// <param name="accountId">accountId</param>
        /// <returns></returns>
        public async Task<IFilethisGenerateIdResponse> CreateNewConnectionAsync(IRequestConnection requestConnection, string accountId)
        {
            var createConnectionRequest = this.CreatingConnectionModel(requestConnection);
            var request = new RestRequest("accounts/" + accountId + "/connections", Method.POST);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(createConnectionRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisGenerateIdResponse>(responseResult.Content);
        }

        //// GET - Get all source connections by AccountId

        /// <summary>
        /// GetConnectionsByAccountIdAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns></returns>
        public async Task<IList<FilethisConnectionResponse>> GetConnectionsAsync(string accountId)
        {
            var requestString = string.Format("/accounts/{0}/connections", accountId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<List<FilethisConnectionResponse>>(responseResult.Content);
        }

        //// GET - Get the given source connection by AccountId and connectionId

        /// <summary>
        /// GetConnectionAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns>response</returns>
        public async Task<IFilethisConnectionResponse> GetConnectionAsync(string accountId, string connectionId)
        {
            var requestString = string.Format("/accounts/{0}/connections/{1}", accountId, connectionId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisConnectionResponse>(responseResult.Content);
        }

        //// PUT - Update the given source connection by AccountId and connectionId

        /// <summary>
        /// UpdateConnectionAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns></returns>
        public async Task<IFilethisConnectionResponse> UpdateConnectionAsync(string accountId, string connectionId)
        {
            var updateConnectionRequest = new FilethisRequestConnectionUpdate(); //TODO Assign model mapping
            var requestString = string.Format("/accounts/{0}/connections/{1}", accountId, connectionId);
            var request = new RestRequest(requestString, Method.PUT);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(updateConnectionRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisConnectionResponse>(responseResult.Content);
        }

        //// DELETE - Delete the given source connection by AccountId and connectionId

        /// <summary>
        /// DeleteConnectionAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns>DeleteConnectionAsync</returns>
        public async Task<bool> DeleteConnectionAsync(string accountId, string connectionId)
        {
            var requestString = string.Format("/accounts/{0}/connections/{1}?documents=true", accountId, connectionId);
            var request = new RestRequest(requestString, Method.DELETE);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if(responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return false;
        }

        //// POST - Initiate a fetch for the given source connection

        /// <summary>
        /// FetchConnectionAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns>true or false</returns>
        public async Task<IFilethisGenerateIdResponse> FetchConnectionAsync(string accountId, string connectionId)
        {
            IFilethisGenerateIdResponse response = new FilethisGenerateIdResponse();
            var requestString = string.Format("/accounts/{0}/connections/{1}/fetch", accountId, connectionId);
            var request = new RestRequest(requestString, Method.POST);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.OK)
            {
                response.Message = "Request has been sent for sync.";
                return response;
            }

            return JsonConvert.DeserializeObject<IFilethisGenerateIdResponse>(responseResult.Content);
        }

        public async Task<dynamic> GetChanges(string fileThisToken)
        {
            var requestString = string.Format("/changes");
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("X-FileThis-Session", FileThisToken);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisConnectionResponse>(responseResult.Content);
        }

        //// GET - Get all source connections 

        /// <summary>
        /// GetConnectionsAsync
        /// </summary>
        /// <returns></returns>
        public async Task<IList<FilethisConnectionResponse>> GetConnectionsAsync()
        {
            var request = new RestRequest("/connections", Method.GET);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<List<FilethisConnectionResponse>>(responseResult.Content);
        }

        //// POST - Create a source connection

        /// <summary>
        /// CreateConnectionAsync
        /// </summary>
        /// <returns></returns>
        public async Task<FilethisGenerateIdResponse> CreateNewConnectionAsync()
        {
            var request = new RestRequest("/connections", Method.POST);
            //request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.AddHeader("X-FileThis-Session", FileThisToken);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisGenerateIdResponse>(responseResult.Content);
        }

        //// GET - Get the given source connection

        /// <summary>
        /// GetConnectionByConnectionIdAsync
        /// </summary>
        /// <param name="connectionId">connectionId</param>
        /// <returns></returns>
        public async Task<IFilethisConnectionResponse> GetConnectionAsync(string connectionId)
        {
            var requestString = string.Format("/connections/{0}", connectionId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisConnectionResponse>(responseResult.Content);
        }

        //// PUT - Update the given source connection by connectionId

        /// <summary>
        /// UpdateConnectionAsync
        /// </summary>
        /// <param name="connectionId">connectionId</param>
        /// <returns></returns>
        public async Task<IFilethisConnectionResponse> UpdateConnectionAsync(string connectionId)
        {
            var updateConnectionRequest = new FilethisRequestConnectionUpdate(); //TODO Assign model mapping
            var requestString = string.Format("/connections/{0}", connectionId);
            var request = new RestRequest(requestString, Method.PUT);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(updateConnectionRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisConnectionResponse>(responseResult.Content);
        }

        //// DELETE - Delete the given source connection by connectionId

        /// <summary>
        /// DeleteConnectionAsync
        /// </summary>
        /// <param name="connectionId">connectionId</param>
        /// <returns>DeleteConnectionAsync</returns>
        public async Task<bool> DeleteConnectionAsync(string connectionId)
        {
            var requestString = string.Format("/connections/{0}", connectionId);
            var request = new RestRequest(requestString, Method.DELETE);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return false;
        }

        //// POST - Initiate a fetch for the given source connection by connectionID

        /// <summary>
        /// FetchConnectionAsync
        /// </summary>
        /// <param name="connectionId">connectionId</param>
        /// <returns>true or false</returns>
        public async Task<bool> FetchConnectionAsync(string connectionId)
        {
            var requestString = string.Format("/connections/{0}/fetch", connectionId);
            var request = new RestRequest(requestString, Method.POST);
            request.AddHeader
                       ("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return false;
        }

        #endregion

        #region Accounts
        //// GET - Get all user account

        /// <summary>
        /// GetAccountsAsync
        /// </summary>
        /// <returns></returns>
        public async Task<IList<FilethisAccountResponse>> GetAccountsAsync()
        {
            var request = new RestRequest("/accounts", Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<List<FilethisAccountResponse>>(responseResult.Content);
        }

        //// GET - Get the given user account
        public async Task<FilethisAccountResponse> GetAccountAsync(string accountId)
        {
            var requestString = string.Format("/accounts/{0}", accountId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<FilethisAccountResponse>(responseResult.Content);
            }

            return null;
        }

        //// POST- Create a user account

        /// <summary>
        /// CreateAccountFilethis
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>response</returns>
        public async Task<IFilethisGenerateIdResponse> CreateNewAccountAsync(string applicantId)
        {
            var createAccountRequest = this.CreatingAccountModel(applicantId);
            var request = new RestRequest("/accounts", Method.POST);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(createAccountRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisGenerateIdResponse>(responseResult.Content);
        }

        //// DELETE- Delete the given user account

        /// <summary>
        /// DeleteAccountAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns></returns>
        public async Task<bool> DeleteAccountAsync(string accountId)
        {
            var requestString = string.Format("/accounts/{0}", accountId);
            var request = new RestRequest(requestString, Method.DELETE);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if(responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return false;
        }

        #endregion

        #region Token

        //// POST - Create a user access token

        /// <summary>
        /// CreateTokenAsync
        /// </summary>
        /// <returns></returns>
        public async Task<IFilethisTokenResponse> CreateTokenAsync(string accountId)
        {
            var createTokenRequest = this.CreatingTokenModel();
            var requestString = string.Format("accounts/{0}/tokens", accountId);
            var request = new RestRequest(requestString, Method.POST);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(createTokenRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisTokenResponse>(responseResult.Content);
        }

        //// DELETE - Delete the given user access token
        public async Task<bool> DeleteTokenAsync(string accountId, string tokenId)
        {
            var requestString = string.Format("accounts/{0}/tokens/{1}", accountId, tokenId);
            var request = new RestRequest(requestString, Method.DELETE);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if(responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return false;
        }

        //// DELETE - Delete the given user access token

        /// <summary>
        /// DeleteTokenAsync
        /// </summary>
        /// <param name="tokenId">tokenId</param>
        /// <returns>true or false</returns>
        public async Task<bool> DeleteTokenAsync(string tokenId)
        {
            var requestString = string.Format("/tokens/{0}", tokenId);
            var request = new RestRequest(requestString, Method.DELETE);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return false;
        }


        #endregion

        #region Partners
        //// GET All partners

        /// <summary>
        /// GetPartnersFilethis
        /// </summary>
        /// <returns></returns>
        public async Task<IList<FilethisPartnerResponse>> GetPartnersFilethisAsync()
        {
            var request = new RestRequest("/partners", Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            var taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client
                .ExecuteAsync(request, restResponse => 
                                        taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<List<FilethisPartnerResponse>>(responseResult.Content);
        }

        /// <summary>
        /// GetPartnerByPartnerIdAsync
        /// </summary>
        /// <param name="partnerId">partnerId</param>
        /// <returns></returns>
        public async Task<FilethisPartnerResponse> GetPartnerByPartnerIdAsync(string partnerId)
        {
            var requestString = string.Format("/partners/{0}", partnerId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisPartnerResponse>(responseResult.Content);
        }

        //// PUT Update partner details

        /// <summary>
        /// UpdatePartnerAsync
        /// </summary>
        /// <param name="partnerId">partnerId</param>
        /// <returns></returns>
        public async Task<bool> UpdatePartnerAsync(string partnerId)
        {
            var createPartnerUpdateRequest = this.CreatingPartnerUpdateRequest();
            var requestString = string.Format("/partners/{0}", partnerId);
            var request = new RestRequest(requestString, Method.PUT);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(createPartnerUpdateRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if(responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        #endregion

        #region Interactions

        //// [GET] All Interactions

        /// <summary>
        /// GetInteractionAsync
        /// </summary>
        /// <returns></returns>
        public async Task<IFilethisInteractionResponse> GetInteractionAsync(string accountId, string connectionId)
        {
            var requestString = string.Format("/accounts/{0}/connections/{1}/interactions", accountId, connectionId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            IFilethisInteractionResponse result = new FilethisInteractionResponse();
            var response = JsonConvert.DeserializeObject<List<FilethisInteractionResponse>>(responseResult.Content);
            result = response[0];
            return result;
        }

        //// [GET] Interaction by given InteractionId

        /// <summary>
        /// GetInteractionAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <param name="interactionId">interactionId</param>
        /// <returns></returns>
        public async Task<IFilethisInteractionResponse> GetInteractionByInteractionIdAsync(string accountId, string connectionId, string interactionId)
        {
            var requestString = string.Format("/accounts/{0}/connections/{1}/interactions/{2}", accountId, connectionId, interactionId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisInteractionResponse>(responseResult.Content);
        }

        //// [PUT] Interaction Response

        /// <summary>
        /// GetInteractionAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <param name="interactionId">interactionId</param>
        /// <returns>True or False</returns>
        public async Task<bool> InteractionResponseAsync(IRequestInteraction requestModel)
        {
            var interactionRequest = this.CreatingInteractionRequest(requestModel);
            var requestString = string.Format("/accounts/{0}/connections/{1}/interactions/{2}/response",
                            requestModel.AccountId, requestModel.ConnectionId, requestModel.InteractionId);
            var request = new RestRequest(requestString, Method.PUT);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(interactionRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if(responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            {
                Logger.Error($"Error while InteractionResponseAsync: ConnectionID {requestModel.ConnectionId} FileThis Expection {responseResult.ErrorException}");
            }
            return false;
        }

        #endregion

        #region Sources
        //// GET - Get all sources

        /// <summary>
        /// GetSourcesAsync
        /// </summary>
        /// <returns></returns>
        public async Task<IList<FilethisSourceResponse>> GetSourcesAsync()
        {
            var request = new RestRequest("/sources", Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<List<FilethisSourceResponse>>(responseResult.Content);
        }

        #endregion

        #region Documents

        //// GET - Get all documents

        /// <summary>
        /// GetDocumentsAsync
        /// </summary>
        /// <returns></returns>
        public async Task<IList<FilethisDocumentResponse>> GetDocumentsAsync(string accountId)
        {
            var requestString = string.Format("/accounts/{0}/documents",accountId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<List<FilethisDocumentResponse>>(responseResult.Content);
        }

        //// GET - Get the given document

        /// <summary>
        /// GetDocumentAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="documentId">documentId</param>
        /// <returns></returns>
        public async Task<IFilethisDocumentResponse> GetDocumentAsync(string accountId, string documentId)
        {
            var requestString = string.Format("/accounts/{0}/documents/{1}", accountId, documentId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisDocumentResponse>(responseResult.Content);
        }

        //// DELETE - Delete the given document
        /// <summary>
        /// DeleteDocumentAsync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="documentId">documentId</param>
        /// <returns></returns>
        public async Task<bool> DeleteDocumentAsync(string accountId, string documentId)
        {
            var requestString = string.Format("/accounts/{0}/documents/{1}", accountId, documentId);
            var request = new RestRequest(requestString, Method.DELETE);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return false;
        }

        //// GET - Get all documents

        /// <summary>
        /// GetDocumentsAsync
        /// </summary>
        /// <returns></returns>
        public async Task<IList<FilethisDocumentResponse>> GetDocumentsAsync()
        {
            var request = new RestRequest("/documents", Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<List<FilethisDocumentResponse>>(responseResult.Content);
        }

        //// GET - Get the given document

        /// <summary>
        /// GetDocumentAsync
        /// </summary>
        /// <param name="documentId">documentId</param>
        /// <returns></returns>
        public async Task<IFilethisDocumentResponse> GetDocumentAsync(string documentId)
        {
            var requestString = string.Format("/documents/{0}", documentId);
            var request = new RestRequest(requestString, Method.GET);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<FilethisDocumentResponse>(responseResult.Content);
        }

        //// DELETE - Delete the given document

        /// <summary>
        /// DeleteDocumentAsync
        /// </summary>
        /// <param name="documentId">documentId</param>
        /// <returns></returns>
        public async Task<bool> DeleteDocumentAsync(string documentId)
        {
            var requestString = string.Format("/documents/{0}", documentId);
            var request = new RestRequest(requestString, Method.DELETE);
            request.AddHeader("Authorization", "Basic " + this.EncodedAuthenticationString);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = this.Client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return false;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IFilethisRequestAccountCreate CreatingAccountModel(string applicantId)
        {
            return new FilethisRequestAccountCreate
            {
                PartnerAccountId = applicantId
            };
        }

        /// <summary>
        /// CreatingTokenModel
        /// </summary>
        /// <returns></returns>
        private IFilethisRequestTokenCreate CreatingTokenModel()
        {
            return new FilethisRequestTokenCreate
            {
                ExpiresIn = this.Configuration.AccountTokenExpiration
            };
        }

        /// <summary>
        /// CreatingConnectionModel
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>response</returns>
        private IFilethisRequestConnectionCreate CreatingConnectionModel(IRequestConnection request)
        {
            return new FilethisRequestConnectionCreate
            {
                Username = request.Username,
                Password = request.Password,
                SourceId = Convert.ToInt64(request.SourceId),
                HistoricalPeriod = this.Configuration.HistoricalPeriod.TaxReturnMonths //// TODO once category finalize
            };
        }

        /// <summary>
        /// CreatingInteractionRequest
        /// </summary>
        /// <param name="interactionId">interactionId</param>
        /// <param name="partId">partId</param>
        /// <param name="partValue">partValue</param>
        /// <returns></returns>
        private IFilethisRequestInteractions CreatingInteractionRequest(IRequestInteraction request)
        {
            var parts = new List<FileThisInteractiveResponsePart>();
            foreach (var item in request.Inputs)
            {
                var part = new FileThisInteractiveResponsePart()
                {
                    Id = item.Key,
                    Value = item.Value
                };

                part.Id = part.Id.Replace("\"",""); //remove " character from part
                parts.Add(part);
            }

            return new FilethisRequestInteractions
            {
                id = request.InteractionId,
                parts = parts,
                version = "1.0.0"
            };
        }

        /// <summary>
        /// CreatingPartnerUpdateRequest
        /// </summary>
        /// <returns></returns>
        private IFilethisRequestPartnerUpdate CreatingPartnerUpdateRequest()
        {
            return new FilethisRequestPartnerUpdate();
        }

        /////// <summary>
        /////// SetHistoricalPeriod
        /////// </summary>
        /////// <param name="sourceId">sourceId</param>
        /////// <returns></returns>
        ////private int SetHistoricalPeriod(long sourceId)
        ////{
        ////    if(sourceId > 0)
        ////    {
        ////        var sourceData = this.SourceRepository.GetSourceByIdAysnc(sourceId.ToString());
        ////        if (sourceData.Result != null)
        ////        {
        ////            if (sourceData.Result.Tags.Any(x => x.Contains("PaystubAndW2s")))
        ////            {
        ////                return this.Configuration.HistoricalPeriod.PayStubsMonths;
        ////            }
        ////        }
        ////    }
        ////    return this.Configuration.HistoricalPeriod.TaxReturnMonths;
        ////}

        #endregion
    }
}
