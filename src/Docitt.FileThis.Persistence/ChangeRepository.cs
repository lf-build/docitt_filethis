﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Docitt.FileThis.Persistence
{
    public class ChangeRepository :
        MongoRepository<IChange, Change>, IChangeRepository
    {
        static ChangeRepository()
        {
            BsonClassMap.RegisterClassMap<Change>(map =>
            {
                map.AutoMap();

                var type = typeof(Change);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public ChangeRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "change")
        {
            CreateIndexIfNotExists("unique_key", 
                Builders<IChange>
                .IndexKeys
                .Ascending(change => change.ChangeId), true);

            CreateIndexIfNotExists("Resource_AccountId_ConnectionId",
                Builders<IChange>
                .IndexKeys
                .Ascending(change => change.Resource.AccountId)
                .Ascending(change => change.Resource.ConnectionId)
                , false);
        }

        /// <summary>
        /// GetChanges
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>Change Collection</returns>
        public async Task<IList<IChange>> GetChanges()
        {
            return await Collection
                .AsQueryable()
                .ToListAsync();
        }

        /// <summary>
        /// GetChanges
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        public async Task<IList<IChange>> GetChanges(string accountId, string connectionId)
        {
            return await Collection
                            .Find(x => x.Resource.AccountId.Equals(accountId) && x.Resource.ConnectionId.Equals(connectionId))
                            .ToListAsync<IChange>();
        }

        /// <summary>
        /// GetLatestChange
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        public async Task<IChange> GetLatestChange(string accountId, string connectionId)
        {
            return await Collection
                         .Find(x => x.Resource.AccountId.Equals(accountId) && x.Resource.ConnectionId.Equals(connectionId)
                                && x.Resource.State != null)
                         .SortByDescending(x => x.ChangeId)
                         .Limit(1)    //TODO: This is not required as FirstOrDefault will return only first row
                         .FirstOrDefaultAsync();
        }

        /// <summary>
        /// AddChanges
        /// </summary>
        /// <param name="changes">changes</param>
        /// <returns>bool</returns>
        public async Task<bool> AddChanges(IEnumerable<IChange> changes)
        {
            try
            {
                await Collection
                        .InsertManyAsync(changes);
            }
            catch (Exception)
            {
                foreach (var item in changes)
                {
                   await this.AddChange(item);
                }
            }

            return true;
        }

        /// <summary>
        /// change
        /// </summary>
        /// <param name="change"></param>
        /// <returns></returns>
        public async Task<bool> AddChange(IChange change)
        {
            await Collection
                    .InsertOneAsync(change);

            return true;
        }
    }
}
