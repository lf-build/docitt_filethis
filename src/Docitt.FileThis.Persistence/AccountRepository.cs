﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using System.Collections.Generic;
using System.Linq;
using System;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;

namespace Docitt.FileThis.Persistence
{
    public class AccountRepository :
        MongoRepository<IAccount, Account>, IAccountRepository
    {
        static AccountRepository()
        {
            BsonClassMap.RegisterClassMap<Account>(map =>
            {
                map.AutoMap();

                var type = typeof(Account);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Connection>(map =>
            {
                map.AutoMap();

                var type = typeof(Connection);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public AccountRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "account")
        {
            CreateIndexIfNotExists("tenant_id", Builders<IAccount>.IndexKeys
                .Ascending(account => account.TenantId), false);

            CreateIndexIfNotExists("applicant_id", Builders<IAccount>.IndexKeys
                .Ascending(account => account.TenantId)
                .Ascending(account => account.ApplicantId), false);

            CreateIndexIfNotExists("filethis_account_id", Builders<IAccount>.IndexKeys
                .Ascending(account => account.TenantId)
                .Ascending(account => account.FileThisAccountId), false);
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <returns>Account Collection</returns>
        public async Task<IList<IAccount>> GetAccounts()
        {
            return await Collection.AsQueryable().ToListAsync();
        }


        /// <summary>
        /// GetAccountById
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>accounts</returns>
        public async Task<IAccount> GetAccountByAccountId(string accountId)
        {
            return (await Collection.FindAsync(account => account.FileThisAccountId == accountId && account.IsActive == true)).FirstOrDefault();
        }

        /// <summary>
        /// GetAccountByApplicant
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>accounts</returns>
        public async Task<IAccount> GetAccountByApplicant(string applicationNumber, string applicantId)
        {
            return (await Collection.FindAsync(
                    account => 
                        account.ApplicationNumber == applicationNumber &&
                        account.ApplicantId == applicantId && 
                        account.IsActive == true)).FirstOrDefault();
        }

        /// <summary>
        /// GetAccountIdApplicantId
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>accounts</returns>
        public async Task<string> GetAccountIdApplicantId(string applicantId)
        {
            var accountData = (await Collection.FindAsync(account => account.ApplicantId == applicantId && account.IsActive == true)).FirstOrDefault();

            if (accountData != null)
                return accountData.FileThisAccountId;
            else
                return string.Empty;
        }

        /// <summary>
        /// GetConnectionByApplicantId
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>accounts</returns>
        public async Task<string> GetConnectionId(string applicantId, string sourceId)
        {
            var connectionId = string.Empty;
            (await Collection.FindAsync(account => account.TenantId == TenantService.Current.Id &&
                                        account.ApplicantId == applicantId && account.IsActive == true))
                .FirstOrDefault<IAccount>().Connection.ForEach(connection =>
                {
                    if (connection.SourceId == sourceId)
                        connectionId = connection.ConnectionId;
                });

            //Query.Where(account => account.TenantId == TenantService.Current.Id && account.ApplicantId == applicantId)
            //    .FirstOrDefault<IAccount>().Connection.ForEach(connection =>
            //    {
            //        if (connection.SourceId == sourceId)
            //            connectionId = connection.ConnectionId;
            //    });

            return connectionId;
        }


        /// <summary>
        /// AddAccount
        /// </summary>
        /// <param name="account">account</param>
        /// <returns>bool</returns>
        public async Task<bool> AddAccount(IAccount account)
        {
            account.TenantId = TenantService.Current.Id;
            await Collection.InsertOneAsync(account);

            return true;
        }

        /// <summary>
        /// UpdateAccount
        /// </summary>
        /// <param name="account">account</param>
        /// <returns>bool</returns>
        public async Task<bool> UpdateAccount(IAccount account)
        {
            return (await Collection.UpdateOneAsync(
                    Builders<IAccount>.Filter
                        .Where(objAccount => objAccount.TenantId == account.TenantId
                        && objAccount.FileThisAccountId == account.FileThisAccountId
                        && objAccount.ApplicantId == account.ApplicantId),
                    Builders<IAccount>.Update
                        .Set(objAccount => objAccount.IsActive, account.IsActive)
                        .Set(objAccount => objAccount.ModifiedDate, account.ModifiedDate)
            )).IsAcknowledged;
        }

        /// <summary>
        /// GetConnection details given connection Id [but no tenant filter]
        /// </summary>
        /// <param name="connectionId">filethis connectionId</param>
        /// <returns>bool</returns>
        public async Task<IAccount> GetConnection(string connectionId)
        {
            var result = await Collection
                           .Find(x => x.Connection.Any( c=> c.ConnectionId ==connectionId))
                           .FirstOrDefaultAsync<IAccount>();
            
            return result;
        }

        /// <summary>
        /// AddConnection
        /// </summary>
        /// <param name="account">account</param>
        /// <returns>bool</returns>
        public async Task<bool> AddConnection(string applicationNumber, string applicantId, string fileThisAccountId, IConnection connection)
        {
            var builder = Builders<IAccount>.Filter;
            var filter = builder.Where(obj => obj.TenantId == TenantService.Current.Id &&
                               obj.ApplicationNumber == applicationNumber
                               && obj.ApplicantId == applicantId
                               && obj.FileThisAccountId == fileThisAccountId);

            // Update information in database
            // UpdateResult updateResult = Collection.UpdateOne
            // (
            //     filter,
            //     Builders<IAccount>.Update.PushEach("Connection", account.Connection)
            // );

            return (await Collection.UpdateOneAsync
            (
                filter,
                Builders<IAccount>.Update.AddToSet("Connection", connection)
            )).IsAcknowledged;
        }

        /// <summary>
        /// RemoveConnection
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns></returns>
        public async Task<bool> DeleteConnection(string accountId, string connectionId)
        {
            var builder = Builders<IAccount>.Filter;
            var filter = builder.Where(obj => obj.TenantId == TenantService.Current.Id
                               && obj.FileThisAccountId == accountId);

            var objAccount = await Collection.Find(filter).FirstOrDefaultAsync<IAccount>();
            if (objAccount == null)
                throw new NotFoundException($"Account {objAccount} cannot be found");

            var connection = objAccount.Connection.FirstOrDefault(x => x.ConnectionId == connectionId);
            var remove = Builders<IAccount>.Update.Pull(x => x.Connection, connection);

            return (await Collection.UpdateOneAsync(filter, remove)).IsAcknowledged;
        }

        /// <summary>
        /// UpdateConnection
        /// </summary>
        /// <param name="account">account</param>
        /// <returns>bool</returns>
        public async Task<bool> UpdateConnection(string applicantId, string fileThisAccountId, IConnection connection)
        {
            var builder = Builders<IAccount>.Filter;
            var filter = builder.Where(obj => obj.TenantId == TenantService.Current.Id
                                && obj.ApplicantId == applicantId
                                && obj.FileThisAccountId == fileThisAccountId);

            //// Get the list of items linked for given applicant
            var objAccount = Collection.Find(filter).FirstOrDefaultAsync<IAccount>().Result;

            if (objAccount == null)
                throw new NotFoundException($"Account {objAccount} cannot be found");

            //// locate the account using accountId and update the balances
            var connections = objAccount.Connection;
            var connectionToUpdate = connections.FirstOrDefault(
                    objConnection => objConnection.ConnectionId == connection.ConnectionId);

            if (connectionToUpdate == null)
                throw new NotFoundException($"Connection {connectionToUpdate} not found");

            // connectionToUpdate.ConnectionId = connection.ConnectionId;
            // connectionToUpdate.InteractionId = connection.InteractionId;
            // connectionToUpdate.SourceId = connection.SourceId;
            // connectionToUpdate.SourceName = connection.SourceName;
            connectionToUpdate.SourceLogoUrl = connection.SourceLogoUrl;
            connectionToUpdate.Tags = connection.Tags;
            connectionToUpdate.Info = connection.Info;
            // connectionToUpdate.Username = connection.Username;
            connectionToUpdate.State = connection.State;
            connectionToUpdate.DocumentCount = connection.DocumentCount;
            connectionToUpdate.Tries = connection.Tries;
            connectionToUpdate.Period = connection.Period;
            connectionToUpdate.FetchAll = connection.FetchAll;
            connectionToUpdate.Enabled = connection.Enabled;
            connectionToUpdate.Validation = connection.Validation;
            connectionToUpdate.HistoricalPeriod = connection.HistoricalPeriod;
            // connectionToUpdate.CheckedDate = connection.CheckedDate;
            // connectionToUpdate.AttemptDate = connection.AttemptDate;
            // connectionToUpdate.SuccessDate = connection.SuccessDate;
            // connectionToUpdate.CreatedDate = connection.CreatedDate;
            connectionToUpdate.KickOffDate = connection.KickOffDate;

            //// Update information in database
            return (await Collection.UpdateOneAsync
            (
                    filter,
                    Builders<IAccount>.Update.Set(a => a.Connection, connections)
            )).IsAcknowledged;
        }

        /// <summary>
        /// RemoveAccountByAccountIds
        /// </summary>
        /// <param name="accountIds">accountIds</param>
        /// <returns>bool</returns>
        public async Task<bool> RemoveAccountByAccountIds(List<string> accountIds)
        {
            return (await Collection.DeleteManyAsync(
                    Builders<IAccount>.Filter
                        .Where(account => accountIds.Contains(account.FileThisAccountId))
                )).IsAcknowledged;
        }
    }
}
