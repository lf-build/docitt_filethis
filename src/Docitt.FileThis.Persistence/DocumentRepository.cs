﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Services;
using MongoDB.Bson.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Docitt.FileThis.Persistence
{
    public class DocumentRepository :
        MongoRepository<IDocument, Document>, IDocumentRepository
    {
        static DocumentRepository()
        {
            BsonClassMap.RegisterClassMap<Document>(map =>
            {
                map.AutoMap();

                var type = typeof(Document);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public DocumentRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "document")
        {
            CreateIndexIfNotExists("tenant_id", Builders<IDocument>.IndexKeys
               .Ascending(docuement => docuement.TenantId), false);

            CreateIndexIfNotExists("unique_key", Builders<IDocument>.IndexKeys
                .Ascending(docuement => docuement.DocumentId), true);

            CreateIndexIfNotExists("connection_id", Builders<IDocument>.IndexKeys
                .Ascending(docuement => docuement.DocumentId)
                .Ascending(docuement => docuement.ConnectionId), true);
        }

        /// <summary>
        /// AddDocuments
        /// </summary>
        /// <param name="documentList">documentList</param>
        /// <returns>true or false</returns>
        public async Task<bool> AddDocuments(IList<IDocument> documentList)
        {
            foreach (var item in documentList)
            {
                item.TenantId = TenantService.Current.Id;
            }
            await Collection.InsertManyAsync(documentList);

            return true;
        }

        /// <summary>
        /// AddDocument
        /// </summary>
        /// <param name="document">document</param>
        /// <returns></returns>
        public async Task<bool> AddDocument(IDocument document)
        {
            document.TenantId = TenantService.Current.Id;
            await Collection.InsertOneAsync(document);

            return true;
        }

        /// <summary>
        /// UpdateDocument
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public async Task<bool> UpdateDocument(IDocument document)
        {
            return (await Collection.UpdateOneAsync(
                   Builders<IDocument>.Filter
                       .Where(objDocument => objDocument.TenantId == document.TenantId
                       && objDocument.ConnectionId == document.ConnectionId),
                   Builders<IDocument>.Update
                       .Set(objDocument => objDocument.IsDeleted, document.IsDeleted)
                       .Set(objDocument => objDocument.FileThisModifiedDate, document.FileThisModifiedDate)
           )).IsAcknowledged;
        }

        public async Task<bool> UpdateDocuments(string connectionId, DateTimeOffset? date)
        {
            return (await Collection.UpdateManyAsync(Builders<IDocument>.Filter
                       .Where(objDocument => objDocument.TenantId == this.TenantService.Current.Id
                       && objDocument.ConnectionId == connectionId), Builders<IDocument>.Update
                       .Set(objDocument => objDocument.IsDeleted, true)
                       .Set(objDocument => objDocument.FileThisModifiedDate, date)
                )).IsAcknowledged;
        }

        /// <summary>
        /// GetDocuments
        /// </summary>
        /// <param name="connectionId">connectionId</param>
        /// <returns>Document List</returns>
        public async Task<IList<IDocument>> GetDocuments(string connectionId, bool flag)
        {
            var documentList = new List<IDocument>();

            if (flag)
            {
                documentList = (await Collection.FindAsync(document => document.ConnectionId.Equals(connectionId)
                                && document.IsDeleted == false))
                                .ToList();
                
            }
            else
            {
                documentList = (await Collection.FindAsync(document => document.ConnectionId.Equals(connectionId)))
                                 .ToList();
            }

            if (documentList == null)
                throw new NotFoundException($"documents {documentList} cannot be found");

            return documentList;
        }

        /// <summary>
        /// GetDocumentById
        /// </summary>
        /// <param name="documentId">documentId</param>
        /// <returns></returns>
        public async Task<IDocument> GetDocumentById(string documentId)
        {
            var documentData = (await Collection.FindAsync(document => document.DocumentId.Equals(documentId)
                               && document.IsDeleted == false))
                               .FirstOrDefault();

            if (documentData == null)
                throw new NotFoundException($"documents {documentData} cannot be found");

            return documentData;
        }
    }
}
