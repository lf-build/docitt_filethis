﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.FileThis.Persistence
{
    public class SourceRepository :
        MongoRepository<ISource, Source>, ISourceRepository
    {
        static SourceRepository()
        {
            BsonClassMap.RegisterClassMap<Source>(map =>
            {
                map.AutoMap();

                var type = typeof(Source);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public SourceRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "source")
        {
            CreateIndexIfNotExists("unique_key", Builders<ISource>.IndexKeys
                .Ascending(source => source.SourceId), true);
        }

        /// <summary>
        /// Get the list of all Institutions
        /// </summary>
        /// <returns>list of Institutions</returns>
        public async Task<IList<ISource>> GetSourcesAsync()
        {
            return await Collection
                .AsQueryable()
                .ToListAsync();
        }

        /// <summary>
        /// GetSources
        /// </summary>
        /// <returns>Source Collection</returns>
        public async Task<IList<ISource>> GetSourcesAsync(string tags, string keyword)
        {
            var builder = Builders<ISource>.Filter;
            var filter = builder.Empty;

            if (!string.IsNullOrEmpty(tags))
                filter = builder.Where(source => source.Tags.Contains(tags));


            if (!string.IsNullOrEmpty(keyword))
                filter = filter & builder.Where(source => source.SourceName.ToLower().Contains(keyword.ToLower()));

            return await Collection.Find(filter).ToListAsync<ISource>();
        }

        /// <summary>
        /// Get Source Information Given Source Id 
        /// </summary>
        /// <param name="sourceId">FileThis Source Id</param>
        /// <returns>Source Information</returns>
        public async Task<ISource> GetSourceByIdAysnc(string sourceId)
        {
            return await Collection
                .Find(source => source.SourceId == sourceId)
                .FirstOrDefaultAsync();
        }
        
        /// <summary>
        /// AddSources
        /// </summary>
        /// <param name="sources">sources</param>
        /// <returns>bool</returns>
        public async Task<bool> AddSources(IEnumerable<ISource> sources)
        {
            await Collection.InsertManyAsync(sources);

            return true;
        }

        /// <summary>
        /// AddSource
        /// </summary>
        /// <param name="source">source</param>
        /// <returns>bool</returns>
        public async Task<bool> AddSource(ISource source)
        {
            await Collection.InsertOneAsync(source);

            return true;
        }

        /// <summary>
        /// UpdateSource
        /// </summary>
        /// <param name="source">source</param>
        /// <returns>bool</returns>
        public async Task<bool> UpdateSource(ISource source)
        {
            return (await Collection.UpdateOneAsync(
                Builders<ISource>.Filter
                    .Where(objSource => objSource.SourceId == source.SourceId),
                Builders<ISource>.Update
                    .Set(objSource => objSource.SourceType, source.SourceType)
                    .Set(objSource => objSource.SourceState, source.SourceState)
                    .Set(objSource => objSource.HomePageUrl, source.HomePageUrl)
                    .Set(objSource => objSource.LogoUrl, source.LogoUrl)
                    .Set(objSource => objSource.Note, source.Note)
                    .Set(objSource => objSource.Info, source.Info)
                    .Set(objSource => objSource.Pattern, source.Pattern)
                    .Set(objSource => objSource.IsNew, source.IsNew)
                    .Set(objSource => objSource.IsPopular, source.IsPopular)
            )).IsAcknowledged;
        }

        /// <summary>
        /// RemoveSourceBySourceIds
        /// </summary>
        /// <param name="sourceIds">sourceIds</param>
        /// <returns>bool</returns>
        public async Task<bool> RemoveSourceBySourceIds(List<string> sourceIds)
        {
            return (await Collection.DeleteManyAsync(
               Builders<ISource>.Filter
                   .Where(source => sourceIds.Contains(source.SourceId))
           )).IsAcknowledged;

        }

        /// <summary>
        /// GetFileThisSourceId
        /// </summary>
        /// <param name="plaidInstitutionId">plaidInstitutionId</param>
        /// <returns>string</returns>
        public async Task<string> GetFileThisSourceIdAysnc(string plaidInstitutionId)
        {
            var sourceData = await Collection
                .Find(source => source.PlaidInstitutionId.Equals(plaidInstitutionId))
                .FirstOrDefaultAsync();

            if(sourceData != null)
            {
                return sourceData.SourceId;
            }
            return string.Empty;
        }
    }
}
