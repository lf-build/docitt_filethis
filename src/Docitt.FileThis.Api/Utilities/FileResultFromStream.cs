﻿#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    /// <summary>
    /// Represents File result from stream
    /// </summary>
    public class FileResultFromStream : ActionResult
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="fileDownloadName"></param>
        /// <param name="fileStream"></param>
        /// <param name="contentType"></param>
        public FileResultFromStream(string fileDownloadName, Stream fileStream, string contentType = null)
        {
            FileDownloadName = fileDownloadName;
            FileStream = fileStream;
            ContentType = contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(fileDownloadName));
        }

        private string ContentType { get; }
        private string FileDownloadName { get; }
        private Stream FileStream { get; }

        /// <summary>
        /// Execute result async
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ExecuteResultAsync(ActionContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = ContentType;
            response.Headers.Add("Content-Disposition", new[] { "attachment; filename=" + FileDownloadName });
            await FileStream.CopyToAsync(context.HttpContext.Response.Body);
            FileStream.Dispose();
        }
    }
}
