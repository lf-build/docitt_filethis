﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;

namespace Docitt.FileThis.Api.Controllers
{
    /// <summary>
    /// PartnerController
    /// </summary>
    public class PartnerController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="partnerService"></param>
        /// <param name="logger"></param>
        public PartnerController(
            IFileThisPartnerService partnerService,
            ILogger logger)
        {
            this.PartnerService = partnerService;
            this.Log = logger;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// Gets PartnerService
        /// </summary>
        private IFileThisPartnerService PartnerService { get; }

        /// <summary>
        /// GetPartners
        /// </summary>
        /// <returns>FilethisPartnerResponse[]</returns>
        [Route("/partners")]
        [HttpGet, HttpPost]
#if DOTNET2
        [ProducesResponseType(typeof(FilethisPartnerResponse[]), 200)]
#endif
        public async Task<IActionResult> GetPartners()
        {
            return await ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetPartners...");
                return this.Ok(await this.PartnerService.GetPartnersAsync());
            });
        }

        /// <summary>
        /// GetPartners
        /// </summary>
        /// <returns>FilethisPartnerResponse[]</returns>
        [Route("/partners/id/{partnerId}")]
        [HttpGet, HttpPost]
#if DOTNET2
        [ProducesResponseType(typeof(FilethisPartnerResponse[]), 200)]
#endif
        public async Task<IActionResult> GetPartnersByID(string partnerId)
        {
            return await ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetPartnersByID...");
                return this.Ok(await this.PartnerService.GetPartnersByIdAsync(partnerId));
            });
        }
    }
}
