﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Docitt.FileThis.Api.Controllers
{
    /// <summary>
    /// ChangeNotificationController
    /// </summary>
    public class ChangeNotificationController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="changeNotificationService"></param>
        /// <param name="logger"></param>
        public ChangeNotificationController(
            IFileThisNotificationService changeNotificationService,
            ILogger logger)
        {
            this.ChangeNotificationService = changeNotificationService;
            this.Log = logger;    
        }

        private ILogger Log { get; }

        /// <summary>
        /// Gets ChangeNotificationService
        /// </summary>
        private IFileThisNotificationService ChangeNotificationService { get; }

        /// <summary>
        /// GetChanges
        /// </summary>
        /// <returns>IChange</returns>
        [Route("/{accountId}/{connectionId}/change")]
        [HttpGet,HttpPost]
        [ProducesResponseType(typeof(IChange), 200)]
        public async Task<IActionResult> GetChanges(string accountId, string connectionId)
        {
            return await ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetChanges...");
                return this.Ok(await this.ChangeNotificationService.GetChange(accountId, connectionId));
            });
        }

        /// <summary>
        /// GetChangesPull
        /// </summary>
        /// <param name="changes">changes</param>
        /// <returns>NoContentResult</returns>
        [Route("/changes/pull")]
        [HttpPost]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        public Task<IActionResult> GetChangesPull([FromBody]FileThisDataHook changes)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetChangesPull...");
                await this.ChangeNotificationService.InsertChangeNotifications(changes);
                return this.Ok();
            });
        }
    }
}
