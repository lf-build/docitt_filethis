﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Docitt.FileThis.Api.Controllers
{
    /// <summary>
    /// ConnectionController
    /// </summary>
    public class ConnectionController : ExtendedController
    {
        /// <summary>
        /// ConnectionController
        /// </summary>
        /// <param name="connectionService"></param>
        /// <param name="logger"></param>
        public ConnectionController(
            IFileThisConnectionService connectionService,
            ILogger logger)
        {
            this.ConnectionService = connectionService;
            this.Log = logger;
        }

        /// <summary>
        /// Logger 
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// Gets ConnectionService
        /// </summary>
        private IFileThisConnectionService ConnectionService { get; }

        /// <summary>
        /// Get the connection Details
        /// </summary>
        /// <param name="connectionId">filethis connection Id</param>
        /// <returns>retuns connection detail</returns>
        [Route("/connection/{connectionId}")]
        [HttpGet]
        [ProducesResponseType(typeof(IResponseConnection), 200)]
        public Task<IActionResult> GetConnection(string connectionId)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetConnection...");
                return this.Ok(await this.ConnectionService.GetConnection(connectionId));
            });
        }

        /// <summary>
        /// CreateConnection
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>IResponseConnection</returns>
        [Route("/connection/create")]
        [HttpPost]
        [ProducesResponseType(typeof(IResponseConnection), 200)]
        public Task<IActionResult> CreateConnection([FromBody]RequestConnection request)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started CreateConnection...");
                return this.Ok(await this.ConnectionService.ConnectionWorkflowAsync(request));
            });
        }

        /// <summary>
        /// InteractionRequest
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>IResponseConnection</returns>
        [Route("/interaction/create")]
        [HttpPost]
        [ProducesResponseType(typeof(IResponseConnection), 200)]
        public Task<IActionResult> InteractionRequest([FromBody]RequestInteraction request)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started InteractionRequest...");
                return this.Ok(await this.ConnectionService.CreateInteractionAsync(request));
            });
        }

        /// <summary>
        /// InteractionResponse
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>bool</returns>
        [Route("/interaction/response")]
        [HttpPost]
        [ProducesResponseType(typeof(bool), 200)]
        public Task<IActionResult> InteractionResponse([FromBody]RequestInteraction request)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started InteractionResponse...");
                return this.Ok(await this.ConnectionService.InteractionResponseAsync(request));
            });
        }

        /// <summary>
        /// ConnectionCheck
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <param name="applicationNumber">Application Number</param>
        /// <returns>IFilethisConnectionResponse</returns>
        [Route("/{applicantId}/{accountId}/{connectionId}/connectioncheck/{applicationNumber?}")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(IFilethisConnectionResponse), 200)]
        public Task<IActionResult> ConnectionCheck(string applicantId, string accountId, string connectionId, string applicationNumber)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started ConnectionCheck...");
                return this.Ok(await this.ConnectionService.ConnectionCheck(applicantId, accountId, connectionId));
            });
        }

        /// <summary>
        /// GetApplicantDetail
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="applicationNumber">Application Number</param>
        /// <returns>IAccount</returns>
        [Route("/{applicantId}/applicantDetail/{applicationNumber}")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(IAccount), 200)]
        public Task<IActionResult> GetApplicantDetail(string applicantId, string applicationNumber)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetApplicantDetail...");
                return this.Ok(await this.ConnectionService.GetApplicantDetail(applicationNumber, applicantId));
            });
        }

        /// <summary>
        /// ConnectionSync
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <param name="applicationNumber">application number</param>
        /// <returns>string</returns>
        [Route("/accounts/{accountId}/connections/{connectionId}/fetch/{applicationNumber?}")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(string), 200)]
        public Task<IActionResult> ConnectionSync(string accountId, string connectionId, string applicationNumber)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started ConnectionSync...");
                return this.Ok(await this.ConnectionService.GetConnectionFetch(accountId, connectionId));
            });
        }

        /// <summary>
        /// DeleteConnection
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <param name="applicationNumber">application number</param>
        /// <returns>bool</returns>
        [Route("/accounts/{accountId}/connections/{connectionId}/delete/{applicationNumber?}")]
        [HttpDelete]
        [ProducesResponseType(typeof(bool), 200)]
        public Task<IActionResult> DeleteConnection(string accountId, string connectionId, string applicationNumber)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started DeleteConnection...");
                return this.Ok(await this.ConnectionService.DeleteConnectionAsync(accountId, connectionId, applicationNumber));
            });
        }
    }
}
