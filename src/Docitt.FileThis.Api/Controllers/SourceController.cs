﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Docitt.FileThis.Api.Controllers
{
    /// <summary>
    /// PartnerController
    /// </summary>
    public class SourceController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="sourceService"></param>
        /// <param name="logger"></param>
        public SourceController(
            IFileThisSourceService sourceService,
            ILogger logger)
        {
            this.SourceService = sourceService;
            this.Log = logger;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// Gets PartnerService
        /// </summary>
        private IFileThisSourceService SourceService { get; }

        /// <summary>
        /// GetSources
        /// </summary>
        /// <returns>ISource[]</returns>
        [Route("/sources")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(ISource[]), 200)]
        public Task<IActionResult> GetSources()
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetSources...");
                return this.Ok(await this.SourceService.GetSourcesAsync());
            });
        }

        /// <summary>
        /// GetCategorySources
        /// </summary>
        /// <param name="categoryType"></param>
        /// <returns>IResponseSource[]</returns>
        [Route("/category/{categorytype}")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(IResponseSource[]), 200)]
        public Task<IActionResult> GetCategorySources(string categoryType)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetCategorySourcesAsync...");
                return this.Ok(await this.SourceService.SearchSourceAsync(categoryType, string.Empty));
            });
        }

        /// <summary>
        /// Get Institution List filtered by category type and search keywords. To be used in auto complete.
        /// </summary>
        /// <param name="categorytype">TaxReturns or PaystubAndW2s</param>
        /// <param name="keyword">search keywords</param>
        /// <returns>IResponseSource[]</returns>
        [Route("/category/{categorytype}/search/{keyword}")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(IResponseSource[]), 200)]
        public Task<IActionResult> GetCategorySearch(string categorytype, string keyword)
        {
            return ExecuteAsync(async () =>
           {
               this.Log.Info("Started GetCategorySearchAsync...");
               return this.Ok(await this.SourceService.SearchSourceAsync(categorytype, keyword));
           });
        }

        /// <summary>
        /// Get Institution Details given Institution Id
        /// </summary>
        /// <returns>IResponseSourceDetail</returns>
        [Route("/source/{sourceId}")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(IResponseSourceDetail), 200)]
        public async Task<IActionResult> GetSourceInformationById(string sourceId)
        {
            return await ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetSourceById...");
                var source = await this.SourceService.GetSourceByIdAsync(sourceId);
                if (source != null)
                    return this.Ok(source);
                else
                    return new NoContentResult();
            });
        }

        /// <summary>
        /// GetFileThisId
        /// </summary>
        /// <param name="plaidInstitutionId">plaidInstitutionId</param>
        /// <returns>plaid ins_id</returns>
        [Route("/plaidInstitutionId/{plaidInstitutionId}/check")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(string), 200)]
        public Task<IActionResult> GetFileThisSourceId(string plaidInstitutionId)
        {
            return ExecuteAsync(async () =>
           {
               this.Log.Info("Started CheckPlaidInstitutionIdExist...");
               return this.Ok(await this.SourceService.CheckPlaidInstitutionIdExist(plaidInstitutionId));
           });
        }
    }
}
