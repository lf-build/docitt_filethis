﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Docitt.FileThis.Api.Controllers
{
    /// <summary>
    /// DocumentController
    /// </summary>
    public class DocumentController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="documentService"></param>
        /// <param name="logger"></param>
        public DocumentController(
            IFileThisDocumentService documentService,
            ILogger logger)
        {
            this.DocumentService = documentService;
            this.Log = logger;
        }

        private ILogger Log { get; }

        /// <summary>
        /// Gets DocumentService
        /// </summary>
        private IFileThisDocumentService DocumentService { get; }

        /// <summary>
        /// GetDocument
        /// </summary>
        /// <param name="connectionId">connectionId</param>
        /// <returns>response</returns>
        [Route("/connection/{connectionId}/getdocument")]
        [HttpGet,HttpPost]
        [ProducesResponseType(typeof(IDocument[]), 200)]
        public Task<IActionResult> GetDocumentsByConnectionId(string connectionId)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started GetDocument...");
                return this.Ok(await this.DocumentService.GetDocumentsByConnectionId(connectionId));
            });
        }

        /// <summary>
        /// DeleteDocument
        /// </summary>
        /// <param name="documentId">documentId</param>
        /// <returns>bool</returns>
        [Route("/document/{documentId}/delete")]
        [HttpDelete]
        [ProducesResponseType(typeof(bool), 200)]
        public Task<IActionResult> DeleteDocument(string documentId)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started DeleteDocument...");
                return this.Ok(await this.DocumentService.DeleteDocument(documentId));
            });
        }

        /// <summary>
        /// DownloadDocument
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="documentId">documentId</param>
        /// <returns>FileResultFromStream</returns>
        [Route("/{applicantId}/document/{documentId}/download")]
        [HttpGet, HttpPost]
        [ProducesResponseType(typeof(FileResultFromStream), 200)]
        public async Task<IActionResult> DownloadDocument(string applicantId, string documentId)
        {
            this.Log.Info("Started DownloadDocument...");
            var result = await this.DocumentService.DownloadDocument(applicantId, documentId);
            return new FileResultFromStream(result.Item1, result.Item2);
        }

        /// <summary>
        /// UpdateDocuments
        /// </summary>
        /// <param name="connectionId"></param>
        /// <returns>IDocument[]</returns>
        [Route("/connection/{connectionId}/sync")]
        [HttpPut]
        [ProducesResponseType(typeof(IDocument[]), 200)]
        public Task<IActionResult> UpdateDocuments(string connectionId)
        {
            return ExecuteAsync(async () =>
            {
                this.Log.Info("Started UpdateDocuments...");
                return this.Ok(await this.DocumentService.UpdateDocumentAsync(connectionId));
            });
        }
    }
}
