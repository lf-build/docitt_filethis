﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using Docitt.FileThis.Persistence;
using LendFoundry.DocumentManager.Client;
using Docitt.FileThis.Abstractions;

namespace Docitt.FileThis
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittFileThis"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[] { } }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.FileThis.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);            
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddTransient<IConfiguration>(
               provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());
               
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddDocumentManager();

            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            services.AddMongoConfiguration(Settings.ServiceName);       

            //// Repository
            services.AddTransient<ISourceRepository, SourceRepository>();
            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IChangeRepository, ChangeRepository>();
            services.AddTransient<IDocumentRepository, DocumentRepository>();

            // Service
            services.AddTransient<IFileThisClient, FileThisClient>();
            services.AddTransient<IFileThisPartnerService, FileThisPartnerService>();
            services.AddTransient<IFileThisConnectionService, FileThisConnectionService>();
            services.AddTransient<IFileThisSourceService, FileThisSourceService>();
            services.AddTransient<IFileThisDocumentService, FileThisDocumentService>();
            services.AddTransient<IFileThisListener, FileThisListener>();
            services.AddTransient<IFileThisNotificationService, FileThisNotificationService>();
            services.AddTransient<IFileThisNotificationServiceFactory, FileThisNotificationServiceFactory>();
            services.AddTransient<IRepositoryFactory, RepositoryFactory>();
            services.AddTransient<IFileThisClientFactory, FileThisClientFactory>();

            // aspnet mvc related
            services.AddMvc()
                .AddLendFoundryJsonOptions();
			
			// CORS
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT FileThis Service");
            });     
            app.UseErrorHandling();
            app.UseRequestLogging(new RequestLoggingMiddlewareOptions(new string[] {"CustomerId","Secret","UserName","Pin","AccountId","ConnectionId","Password"}));
            app.UseMvc();
            app.UseFileThisListener();
            app.UseConfigurationCacheDependency();
        }
    }
}