﻿using Docitt.FileThis.Abstractions;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Docitt.FileThis.Client
{
    public class DccFileThisServiceFactory : IDccFileThisServiceFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public DccFileThisServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public DccFileThisServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }

        public IDccFileThisService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("filethis");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new DccFileThisService(client);
        }
    }
}
