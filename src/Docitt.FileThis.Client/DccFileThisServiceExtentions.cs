﻿using System;
using Docitt.FileThis.Abstractions;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.FileThis.Client
{
    public static class DccFileThisServiceExtentions
    {
          [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddDccFileThisService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IDccFileThisServiceFactory>(p => new DccFileThisServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IDccFileThisServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDccFileThisService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IDccFileThisServiceFactory>(p => new DccFileThisServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IDccFileThisServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDccFileThisService(this IServiceCollection services)
        {
            services.AddTransient<IDccFileThisServiceFactory>(p => new DccFileThisServiceFactory(p));
            services.AddTransient(p => p.GetService<IDccFileThisServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
