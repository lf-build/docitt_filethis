﻿using Docitt.FileThis.Abstractions;
using LendFoundry.Foundation.Client;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Docitt.FileThis.Client
{
    public class DccFileThisService : IDccFileThisService
    {
        public DccFileThisService(IServiceClient client)
        {
            this.Client = client;
        }

        private IServiceClient Client { get; }

        #region "Sources"
        public async Task<IList<Source>> GetSources()
        {
            return await Client.PostAsync<dynamic, List<Source>>("/", null, true);
        }

        public async Task<IList<ResponseSource>> GetCategorySources(string sourcetype)
        {
            return await Client.PostAsync<dynamic, List<ResponseSource>>($"category/{sourcetype}", null, true);
        }

        public async Task<IList<ResponseSource>> GetCategorySearch(string sourcetype,string keyword)
        {
            return await Client.PostAsync<dynamic, List<ResponseSource>>($"category/{sourcetype}/search/{keyword}", null, true);
        }

        public async Task<IResponseSourceDetail> GetSourceById(string id)
        {
            return await Client.PostAsync<dynamic, ResponseSourceDetail>($"source/{id}", null, true);
        }

        public async Task<string> GetFileThisSourceId(string plaidInstitutionId)
        {
            var response = await Client.PostAsync<dynamic, dynamic>($"/plaidInstitutionId/{plaidInstitutionId}/check", null, true);
            return Convert.ToString(response);                    
        }

        #endregion

        #region "Partner"

        public async Task<IList<FilethisPartnerResponse>> GetPartners()
        {
            return await Client.PostAsync<dynamic, List<FilethisPartnerResponse>>("partners", null, true);
        }
        #endregion

        #region "Connection and Interactions"

        public async Task<IResponseConnection> CreateConnection(IRequestConnection requestData)
        {
            return await Client.PostAsync<IRequestConnection, ResponseConnection>("connection/create", requestData, true);
        }
        
        public async Task<IResponseConnection> InteractionRequest(IRequestInteraction requestData)
        {
            return await Client.PostAsync<IRequestInteraction, ResponseConnection>("interaction/create", requestData, true);
        }

        public async Task<bool> InteractionResponse(IRequestInteraction requestData)
        {
            var response =  await Client.PostAsync<dynamic,dynamic>("interaction/response", requestData, true);
            return response; 
        }

        public async Task<IFilethisConnectionResponse> ConnectionCheck(string applicantId, string accountId, string connectionId)
        {
            return await Client.PostAsync<dynamic, FilethisConnectionResponse>($"{applicantId}/{accountId}/{connectionId}/connectioncheck", null, true);
        }

        public async Task<IAccount> GetApplicantDetail(string applicationNumber, string applicantId)
        {
            var request = new RestRequest($"{applicantId}/applicantDetail/{applicationNumber}", Method.GET);
            return await Client.ExecuteAsync<Account>(request);
        }

        public async Task<bool> DeleteConnection(string accountId, string connectionId)
        {
            var request = new RestRequest($"accounts/{accountId}/connections/{connectionId}/delete", Method.DELETE);
            return await Client.ExecuteAsync<bool>(request);
        }

        #endregion

        #region "Documents"

        public async Task<IList<Document>> GetDocuments(string connectionId)
        {
            return await Client.PostAsync<dynamic,List<Document>>($"connection/{connectionId}/getdocument", null, true);
        }

        public async Task<bool> DeleteDocument(string documentId)
        {
            var request = new RestRequest($"document/{documentId}/delete", Method.DELETE);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<Stream> DownloadDocument(string applicantId, string documentId)
        {
            var request = new RestRequest($"{applicantId}/document/{documentId}/download", Method.POST);
            return await Client.ExecuteAsync<Stream>(request);

            // return await Client.PostAsync<dynamic,Stream>("{applicantId}/document/{documentId}/download", null, true);
        }

        public async Task<IList<Document>> ConnectionSyncDocuments(string connectionId)
        {
           return await Client.PutAsync<dynamic, List<Document>>($"connection/{connectionId}/sync", null, true);
        }


        #endregion

        #region "Change Notifications"

        public async Task<IChange> GetLatestChange(string accountId, string connectionId)
        {
            return await Client.PostAsync<dynamic,Change>($"{accountId}/{connectionId}/change", null, true);
        }

        public async Task<bool> GetChangesPull(FileThisDataHook changes)
        {
             var response =  await Client.PostAsync<FileThisDataHook,dynamic>("changes/pull", changes, true);
             return Convert.ToBoolean(response);
        }

        #endregion
    }
}
