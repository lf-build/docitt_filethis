﻿using LendFoundry.Security.Tokens;

namespace Docitt.FileThis.Abstractions
{
    public interface IDccFileThisServiceFactory
    {
        IDccFileThisService Create(ITokenReader reader);
    }
}
