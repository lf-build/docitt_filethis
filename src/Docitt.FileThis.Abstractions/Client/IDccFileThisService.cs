﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Docitt.FileThis.Abstractions
{
    public interface IDccFileThisService
    {
        #region "Sources"

        Task<IList<Source>> GetSources();

        Task<IList<ResponseSource>> GetCategorySources(string sourcetype);

        Task<IList<ResponseSource>> GetCategorySearch(string sourcetype, string keyword);

        Task<IResponseSourceDetail> GetSourceById(string id);

        Task<string> GetFileThisSourceId(string plaidInstitutionId);

        #endregion

        #region "Partner"

        Task<IList<FilethisPartnerResponse>> GetPartners();

        #endregion

        #region "Connection and Interactions"

        Task<IResponseConnection> CreateConnection(IRequestConnection request);

        Task<IResponseConnection> InteractionRequest(IRequestInteraction request);

        Task<bool> InteractionResponse(IRequestInteraction request);

        Task<IFilethisConnectionResponse> ConnectionCheck(string applicantId, string accountId, string connectionId);

        Task<IAccount> GetApplicantDetail(string applicationNumber, string applicantId);

        Task<bool> DeleteConnection(string accountId, string connectionId);

        #endregion

        #region "Documents"

        Task<IList<Document>> GetDocuments(string connectionId);

        Task<bool> DeleteDocument(string documentId);

        Task<Stream> DownloadDocument(string applicantId, string documentId);

        Task<IList<Document>> ConnectionSyncDocuments(string connectionId);

        #endregion

        #region "Change Notifications"

        Task<IChange> GetLatestChange(string accountId, string connectionId);

        Task<bool> GetChangesPull(FileThisDataHook changes);

        #endregion
    }
}
