﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;
using System;

namespace Docitt.FileThis
{
    /// <summary>
    /// IDocumentRepository
    /// </summary>
    public interface IDocumentRepository : IRepository<IDocument>
    {
        Task<bool> AddDocuments(IList<IDocument> documentList);

        Task<bool> AddDocument(IDocument document);

        Task<bool> UpdateDocument(IDocument document);

        Task<bool> UpdateDocuments(string connectionId, DateTimeOffset? date);

        Task<IList<IDocument>> GetDocuments(string connectionId, bool flag);

        Task<IDocument> GetDocumentById(string documentId);
    }
}
