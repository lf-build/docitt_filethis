﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    /// <summary>
    /// IAccountRepository
    /// </summary>
    public interface IAccountRepository : IRepository<IAccount>
    {
        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <returns>Account Collection</returns>
        Task<IList<IAccount>> GetAccounts();

        /// <summary>
        /// GetAccountById
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>Account</returns>
        Task<IAccount> GetAccountByAccountId(string accountId);

        /// <summary>
        /// GetAccountByApplicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>Account</returns>
        Task<IAccount> GetAccountByApplicant(string applicationNumber, string applicantId);

        /// <summary>
        /// GetAccountIdApplicantId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns></returns>
        Task<string> GetAccountIdApplicantId(string applicantId);

        /// <summary>
        /// AddAccount
        /// </summary>
        /// <param name="account">account</param>
        /// <returns>bool</returns>
        Task<bool> AddAccount(IAccount account);

        /// <summary>
        /// UpdateAccount
        /// </summary>
        /// <param name="account">account</param>
        /// <returns>bool</returns>
        Task<bool> UpdateAccount(IAccount account);

        /// <summary>
        /// GetConnection
        /// </summary>
        /// <param name="connectionId">filethis connection</param>
        /// <returns>bool</returns>
        Task<IAccount> GetConnection(string connectionId);


        /// <summary>
        /// AddConnection
        /// </summary>
        /// <param name="applicationNumber">application number</param>
        /// <param name="applicantId">applicant Id</param>
        /// <param name="fileThisAccountId">filethis account id</param>
        /// <param name="connection">type of IConnection</param>
        /// <returns>bool</returns>
        Task<bool> AddConnection(string applicationNumber, string applicantId, string fileThisAccountId, IConnection connection);

        /// <summary>
        /// UpdateConnection
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="fileThisAccountId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        Task<bool> UpdateConnection(string applicantId, string fileThisAccountId, IConnection connection);

        /// <summary>
        /// RemoveConnection
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns>True or false</returns>
        Task<bool> DeleteConnection(string accountId, string connectionId);

        /// <summary>
        /// RemoveAccountByAccountIds
        /// </summary>
        /// <param name="accountIds">accountIds</param>
        /// <returns>bool</returns>
        Task<bool> RemoveAccountByAccountIds(List<string> accountIds);
    }
}
