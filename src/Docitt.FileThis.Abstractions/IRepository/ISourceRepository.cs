﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    /// <summary>
    /// ISourceRepository
    /// </summary>
    public interface ISourceRepository : IRepository<ISource>
    {
        /// <summary>
        /// GetSources
        /// </summary>
        /// <returns>Source Collection</returns>
        Task<IList<ISource>> GetSourcesAsync();

        /// <summary>
        /// GetSources
        /// </summary>
        /// <param name="tanents">tanents</param>
        /// <param name="tags">tags</param>
        /// <param name="keyword">keyword</param>
        /// <returns>Source Collection</returns>
        Task<IList<ISource>> GetSourcesAsync(string tags, string keyword);

        /// <summary>
        /// GetSourceById
        /// </summary>
        /// <param name="sourceId">sourceId</param>
        /// <returns>Sources</returns>
        Task<ISource> GetSourceByIdAysnc(string sourceId);

        /// <summary>
        /// AddSources
        /// </summary>
        /// <param name="sources">sources</param>
        /// <returns>bool</returns>
        Task<bool> AddSources(IEnumerable<ISource> sources);

        /// <summary>
        /// AddSource
        /// </summary>
        /// <param name="source">source</param>
        /// <returns>bool</returns>
        Task<bool> AddSource(ISource source);

        /// <summary>
        /// UpdateSource
        /// </summary>
        /// <param name="source">source</param>
        /// <returns>bool</returns>
        Task<bool> UpdateSource(ISource source);

        /// <summary>
        /// RemoveSourceBySourceIds
        /// </summary>
        /// <param name="sourceIds">sourceIds</param>
        /// <returns>bool</returns>
        Task<bool> RemoveSourceBySourceIds(List<string> sourceIds);

        /// <summary>
        /// GetFileThisSourceIdAysnc
        /// </summary>
        /// <param name="plaidInstitutionId">plaidInstitutionId</param>
        /// <returns>string</returns>
        Task<string> GetFileThisSourceIdAysnc(string plaidInstitutionId);
    }
}
