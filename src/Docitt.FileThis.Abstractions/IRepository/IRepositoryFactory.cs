﻿using LendFoundry.Security.Tokens;

namespace Docitt.FileThis
{
    public interface IRepositoryFactory
    {
        IChangeRepository CreateChangeRepository(ITokenReader reader);

        IDocumentRepository CreateDocumentRepository(ITokenReader reader);
    }
}
