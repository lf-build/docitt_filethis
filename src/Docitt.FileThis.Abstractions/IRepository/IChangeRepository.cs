﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    /// <summary>
    /// ISourceRepository
    /// </summary>
    public interface IChangeRepository : IRepository<IChange>
    {
        /// <summary>
        /// GetChanges
        /// </summary>
        /// <param name="accountId">accountId</param>
        Task<IList<IChange>> GetChanges();

        /// <summary>
        /// GetChanges
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns>change List</returns>
        Task<IList<IChange>> GetChanges(string accountId, string connectionId);

        /// <summary>
        /// GetLatestChange
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="connectionId">connectionId</param>
        /// <returns>Change</returns>
        Task<IChange> GetLatestChange(string accountId, string connectionId);

        /// <summary>
        /// AddChanges
        /// </summary>
        /// <param name="changes">changes</param>
        /// <returns>bool</returns>
        Task<bool> AddChanges(IEnumerable<IChange> changes);

        /// <summary>
        /// AddChange
        /// </summary>
        /// <param name="change">change</param>
        /// <returns>bool</returns>
        Task<bool> AddChange(IChange change);
    }
}
