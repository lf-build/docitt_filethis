﻿using Docitt.FileThis.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    public interface IFileThisNotificationService
    {
        Task<IChange> GetChange(string accountId, string connectionId);

        Task InsertChangeNotifications(FileThisDataHook changeNotification);

        Task InsertDocuments(string metaData);
    }
}
