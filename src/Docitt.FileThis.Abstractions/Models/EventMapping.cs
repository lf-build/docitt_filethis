﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.FileThis.Abstractions
{
    public class EventMapping
    {
        public string Name { get; set; }

        public string EventName { get; set; }

        public string Data { get; set; }
    }
}
