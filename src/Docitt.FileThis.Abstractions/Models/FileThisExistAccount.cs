﻿namespace Docitt.FileThis
{
    public interface IFileThisExistAccount
    {
        string FileThisAccountId { get; set; }

        string FileThisConnectionId { get; set; }

        bool ApplicantExists { get; set; }
    }
    public class FileThisExistAccount : IFileThisExistAccount
    {
        public string FileThisAccountId { get; set; }

        public string FileThisConnectionId { get; set; }

        public bool ApplicantExists { get; set; }
    }
}
