﻿using Docitt.FileThis.Abstractions;
using System.Collections.Generic;

namespace Docitt.FileThis
{
    public class Configuration : IConfiguration
    {
        public EventMapping[] Events { get; set; }

        public string Url { get; set; }

        public string Version { get; set; }

        public string Key { get; set; }

        public string Secret { get; set; }

        public int AccountTokenExpiration { get; set; }

        public int PartnerId { get; set; }

        public HistoricalPeriod HistoricalPeriod { get; set; }

        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
