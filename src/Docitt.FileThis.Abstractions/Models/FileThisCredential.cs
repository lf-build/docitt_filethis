﻿using Docitt.FileThis.Abstractions;
using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public interface IFileThisCredential
    {
        string Label { get; set; }
        string Name { get; set; }
        string Type { get; set; }
    }

    public class FileThisCredential : IFileThisCredential
    {
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
