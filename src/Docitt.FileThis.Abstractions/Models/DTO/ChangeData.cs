﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public partial class ChangesDataHook
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("subscriptionId")]
        public string SubscriptionId { get; set; }

        [JsonProperty("created")]
        public string Created { get; set; }

        [JsonProperty("resource")]
        public string Resource { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
