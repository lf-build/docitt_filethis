﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.FileThis
{
    public partial class FileThisDataHook
    {
        [JsonProperty("changes")]
        public List<ChangesDataHook> Changes { get; set; }
    }
}
