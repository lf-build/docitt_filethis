﻿namespace Docitt.FileThis
{
    public class HistoricalPeriod
    {
        public int TaxReturnMonths { get; set; }

        public int PayStubsMonths { get; set; }

        public int W2Months { get; set; }
    }
}
