﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    public class DocumentMetaData
    {
            public object MinimumAmountDue { get; set; }
            public object LastPaymentAmount { get; set; }
            public string SourceId { get; set; }
            public string Institution { get; set; }
            public string Filename { get; set; }
            public object EndDate { get; set; }
            public object DueDate { get; set; }
            public string DocumentType { get; set; }
            public string DocumentSubType { get; set; }
            public string DocumentId { get; set; }
            public object StartDate { get; set; }
            public string Date { get; set; }
            public object CurrentBalance { get; set; }
            public DateTime CreatedOn { get; set; }
            public string ConnectionId { get; set; }
            public object ActionDate { get; set; }
            public string AccountType { get; set; }
            public object AccountSubtype { get; set; }
            public object AccountNumber { get; set; }
            public string AccountName { get; set; }
            public string CustomerId { get; set; }
            public object TotalAmountDue { get; set; }
    }
}
