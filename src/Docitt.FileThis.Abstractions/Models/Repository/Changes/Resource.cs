﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    public class Resource
    {
        public string AccountId { get; set; }
        public string ConnectionId { get; set; }
        public string InteractionId { get; set; }
        public string State { get; set; }
    }
}
