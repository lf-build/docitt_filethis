﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.FileThis
{
    public interface IChange: IAggregate
    {
        Resource Resource { get; set; }
        DateTime CreatedDate { get; set; }
        string SubscriberId { get; set; }
        string ChangeId { get; set; }
        string PartnerId { get; set; }
        string SubscriptionId { get; set; }
        string Type { get; set; }
    }
}
