﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;

namespace Docitt.FileThis
{
    public class Change : Aggregate, IChange
    {
        [JsonProperty("resource")]
        public Resource Resource { get; set; }

        [JsonProperty("created")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("subscriberId")]
        public string SubscriberId { get; set; }

        [JsonProperty("changeId")]
        public string ChangeId { get; set; }

        [JsonProperty("partnerId")]
        public string PartnerId { get; set; }

        [JsonProperty("subscriptionId")]
        public string SubscriptionId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
