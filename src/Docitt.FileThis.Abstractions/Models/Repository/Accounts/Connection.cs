﻿using System;

namespace Docitt.FileThis
{
    public class Connection : IConnection
    {
        public string ConnectionId { get; set; }
        public string InteractionId { get; set; }
        public string SourceId { get; set; }
        public string SourceName { get; set; }
        public string SourceLogoUrl { get; set; }
        public string[] Tags { get; set; }
        public string Info { get; set; }
        public string Username { get; set; }
        public string State { get; set; }
        public int DocumentCount { get; set; }
        public int Tries { get; set; }
        public string Period { get; set; }
        public bool FetchAll { get; set; }
        public bool Enabled { get; set; }
        public string Validation { get; set; }
        public int HistoricalPeriod { get; set; }
        public DateTime CheckedDate { get; set; }
        public DateTime AttemptDate { get; set; }
        public DateTime SuccessDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime KickOffDate { get; set; }
    }
}
