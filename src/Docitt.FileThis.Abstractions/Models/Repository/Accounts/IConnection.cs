﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Serializers;

namespace Docitt.FileThis
{
    [BsonSerializer(typeof(ImpliedImplementationInterfaceSerializer<IConnection, Connection>))]
    public interface IConnection
    {
        string ConnectionId { get; set; }
        string InteractionId { get; set; }
        string SourceId { get; set; }
        string SourceName { get; set; }
        string SourceLogoUrl { get; set; }
        string[] Tags { get; set; }
        string Info { get; set; }
        string Username { get; set; }
        string State { get; set; }
        int DocumentCount { get; set; }
        int Tries { get; set; }
        string Period { get; set; }
        bool FetchAll { get; set; }
        bool Enabled { get; set; }
        string Validation { get; set; }
        int HistoricalPeriod { get; set; }
        DateTime CheckedDate { get; set; }
        DateTime AttemptDate { get; set; }
        DateTime SuccessDate { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime KickOffDate { get; set; }
    }
}
