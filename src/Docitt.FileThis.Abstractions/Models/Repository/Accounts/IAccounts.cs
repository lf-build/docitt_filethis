﻿using System;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace Docitt.FileThis
{
    public interface IAccount : IAggregate
    {
        string ApplicantId { get; set; }
        string PartnerCustomerId {get; set;}
        string FileThisAccountId { get; set; }
        string PartnerId { get; set; }
        bool IsActive { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime ModifiedDate { get; set; }
        List<IConnection> Connection { get; set; }
        string ApplicationNumber {get; set;}
    }
}
