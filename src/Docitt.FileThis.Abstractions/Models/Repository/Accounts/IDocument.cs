﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.FileThis
{
    public interface IDocument : IAggregate
    {
        string Name { get; set; }
       string MinimumAmountDue { get; set; }
       string LastPaymentAmount { get; set; }
       string InstitutionId { get; set; }
       string Institution { get; set; }
       string Filename { get; set; }
       string EndDate { get; set; }
       string DueDate { get; set; }
       string DocumentType { get; set; }
       string DocumentSubType { get; set; }
       string DocumentId { get; set; }
       string StartDate { get; set; }
       string Date { get; set; }
       string CurrentBalance { get; set; }
       DateTime CreatedOn { get; set; }
       string ConnectionId { get; set; }
       string ActionDate { get; set; }
       string AccountType { get; set; }
       string AccountSubtype { get; set; }
       string AccountNumber { get; set; }
       string AccountName { get; set; }
       string CustomerId { get; set; }
       string PartnerCustomerId { get; set; }
       string TotalAmountDue { get; set; }
        bool IsDeleted { get; set; }
        string DocittDocumentId { get; set; }
        DateTimeOffset FileThisCreatedDate { get; set; }

        DateTimeOffset? FileThisModifiedDate { get; set; }
      
    }
}
