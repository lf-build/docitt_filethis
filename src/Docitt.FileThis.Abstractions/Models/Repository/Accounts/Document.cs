﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;

namespace Docitt.FileThis
{
    public class Document : Aggregate, IDocument
    {
        public string Name { get; set; }
        public string MinimumAmountDue { get; set; }
        public string LastPaymentAmount { get; set; }
        public string InstitutionId { get; set; }
        public string Institution { get; set; }
        public string Filename { get; set; }
        public string EndDate { get; set; }
        public string DueDate { get; set; }
        public string DocumentType { get; set; }
        public string DocumentSubType { get; set; }
        public string DocumentId { get; set; }
        public string StartDate { get; set; }
        public string Date { get; set; }
        public string CurrentBalance { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ConnectionId { get; set; }
        public string ActionDate { get; set; }
        public string AccountType { get; set; }
        public string AccountSubtype { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string CustomerId { get; set; }
        public string PartnerCustomerId { get; set; }
        public string TotalAmountDue { get; set; }
        public bool IsDeleted { get; set; }
        public string DocittDocumentId { get; set; }
        public DateTimeOffset FileThisCreatedDate { get; set; }
        public DateTimeOffset? FileThisModifiedDate { get; set; }
    }
}
