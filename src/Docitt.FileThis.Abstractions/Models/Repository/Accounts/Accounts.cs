﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace Docitt.FileThis
{
    public class Account : Aggregate, IAccount
    {
        public string ApplicantId { get; set; }
        public string PartnerCustomerId {get; set;}
        public string FileThisAccountId { get; set; }
        public string PartnerId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        [BsonIgnoreIfNull]
        public List<IConnection> Connection { get; set; }
        
        public string ApplicationNumber {get; set;}
    }
}
