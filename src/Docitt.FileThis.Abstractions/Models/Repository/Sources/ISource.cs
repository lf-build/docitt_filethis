﻿using LendFoundry.Foundation.Persistence;

namespace Docitt.FileThis
{
    public interface ISource : IAggregate
    {
        string SourceId { get; set; }
        string SourceName { get; set; }
        string SourceType { get; set; }
        string SourceState { get; set; }
        string HomePageUrl { get; set; }
        string Phone { get; set; }
        string LogoPath { get; set; }
        string Logo { get; set; }
        string LogoUrl { get; set; }
        string Note { get; set; }
        string Info { get; set; }
        string Pattern { get; set; }
        bool IsNew { get; set; }
        bool IsPopular { get; set; }
        string PlaidInstitutionId { get; set; }
        string[] Tags { get; set; }
        string[] Tenants { get; set; }
    }
}
