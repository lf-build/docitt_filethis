﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class Source : Aggregate, ISource
    {
        [JsonProperty("id")]
        public string SourceId { get; set; }

        [JsonProperty("name")]
        public string SourceName { get; set; }

        [JsonProperty("type")]
        public string SourceType { get; set; }

        [JsonProperty("state")]
        public string SourceState { get; set; }

        [JsonProperty("homePageUrl")]
        public string HomePageUrl { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("logoPath")]
        public string LogoPath { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("logoUrl")]
        public string LogoUrl { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }

        [JsonProperty("pattern")]
        public string Pattern { get; set; }

        [JsonProperty("isNew")]
        public bool IsNew { get; set; }

        [JsonProperty("isPopular")]
        public bool IsPopular { get; set; }

        [JsonProperty("plaidInstitutionId")]
        public string PlaidInstitutionId { get; set; }

        public string[] Tags { get; set; }

        public string[] Tenants { get; set; }
    }
}
