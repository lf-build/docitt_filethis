﻿using System;

namespace Docitt.FileThis
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "docitt-filethis";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";

        public static string DocumentManagerEntityType { get; } = Environment.GetEnvironmentVariable($"{Prefix}_DOCUMENTMANAGER_ENTITY_TYPE") ?? "filethis";
    }
}
