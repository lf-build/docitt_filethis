﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisErrorResponse : IFilethisErrorResponse
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
