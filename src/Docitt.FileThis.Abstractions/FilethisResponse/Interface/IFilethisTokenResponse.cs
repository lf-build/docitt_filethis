﻿namespace Docitt.FileThis
{
    public interface IFilethisTokenResponse
    {
         string ExpiresIn { get; set; }
         string Id { get; set; }
         string Token { get; set; }
    }
}
