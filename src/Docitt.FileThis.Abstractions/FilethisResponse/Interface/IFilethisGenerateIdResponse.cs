﻿namespace Docitt.FileThis
{
    public interface IFilethisGenerateIdResponse
    {
         string Id { get; set; }

         string Message { get; set; }

         string Status { get; set; }
    }
}
