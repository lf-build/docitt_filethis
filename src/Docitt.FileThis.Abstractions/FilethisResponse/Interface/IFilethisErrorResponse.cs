﻿namespace Docitt.FileThis
{
    public interface IFilethisErrorResponse
    {
         string Message { get; set; }

         string Status { get; set; }
    }
}
