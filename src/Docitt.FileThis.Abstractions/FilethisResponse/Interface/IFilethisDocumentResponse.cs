﻿using System;

namespace Docitt.FileThis
{
    public interface IFilethisDocumentResponse
    {
        bool Shared { get; set; }

        int PageCount { get; set; }

        DateTime AddedDate { get; set; }

        string Kind { get; set; }

        string MimeType { get; set; }

        string DestinationId { get; set; }

        DateTime DeliveredDate { get; set; }

        string OriginalName { get; set; }

        string AccountId { get; set; }

        DateTime CreatedDate { get; set; }

        int Size { get; set; }

        string DeliveryState { get; set; }

        string Name { get; set; }

        string ConnectionId { get; set; }

        string Id { get; set; }

        DateTime RelevantDate { get; set; }

        DateTime ActionDate { get; set; }

        DateTime OriginalRelevantDate { get; set; }
    }
}
