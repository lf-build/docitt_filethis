﻿using System.Collections.Generic;

namespace Docitt.FileThis
{
    public interface IFilethisInteractionResponse
    {
        string Code { get; set; }
        List<FileThisPart> Parts { get; set; }
        string Id { get; set; }
        string Title { get; set; }
        string LegacyCode { get; set; }
        string Version { get; set; }
    }
}
