﻿namespace Docitt.FileThis
{
    public interface IFilethisPartnerResponse
    {
        object AwsBucket { get; set; }
        string Cors { get; set; }
        string Substitution { get; set; }
        string ApiKey { get; set; }
        bool MetadataOnly { get; set; }
        string ApiSecret { get; set; }
        object AwsKey { get; set; }
        object AwsSecret { get; set; }
        string MetadataVersion { get; set; }
        string DomainName { get; set; }
        string Name { get; set; }
        string AdminAccountId { get; set; }
        string DeliveryUrl { get; set; }
        string HistoricalPeriod { get; set; }
        string Id { get; set; }
    }
}
