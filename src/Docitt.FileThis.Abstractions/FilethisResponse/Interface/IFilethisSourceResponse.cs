﻿namespace Docitt.FileThis
{
    public interface IFilethisSourceResponse
    {
        int Id { get; set; }
        string Name { get; set; }
        string Type { get; set; }
        string State { get; set; }
        string HomePageUrl { get; set; }
        string Phone { get; set; }
        string LogoPath { get; set; }
        string Logo { get; set; }
        string LogoUrl { get; set; }
        string Note { get; set; }
        string Info { get; set; }
        string Pattern { get; set; }
        bool IsNew { get; set; }
        bool IsPopular { get; set; }
    }
}
