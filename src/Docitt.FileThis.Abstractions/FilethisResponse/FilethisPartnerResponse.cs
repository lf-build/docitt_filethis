﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisPartnerResponse : IFilethisPartnerResponse
    {
        [JsonProperty("awsBucket")]
        public object AwsBucket { get; set; }

        [JsonProperty("cors")]
        public string Cors { get; set; }

        [JsonProperty("substitution")]
        public string Substitution { get; set; }

        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }

        [JsonProperty("metadataOnly")]
        public bool MetadataOnly { get; set; }

        [JsonProperty("apiSecret")]
        public string ApiSecret { get; set; }

        [JsonProperty("awsKey")]
        public object AwsKey { get; set; }

        [JsonProperty("awsSecret")]
        public object AwsSecret { get; set; }

        [JsonProperty("metadataVersion")]
        public string MetadataVersion { get; set; }

        [JsonProperty("domainName")]
        public string DomainName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("adminAccountId")]
        public string AdminAccountId { get; set; }

        [JsonProperty("deliveryUrl")]
        public string DeliveryUrl { get; set; }

        [JsonProperty("historicalPeriod")]
        public string HistoricalPeriod { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
