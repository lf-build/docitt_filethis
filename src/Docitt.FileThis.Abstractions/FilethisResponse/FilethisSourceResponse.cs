﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisSourceResponse : IFilethisSourceResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("homePageUrl")]
        public string HomePageUrl { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("logoPath")]
        public string LogoPath { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("logoUrl")]
        public string LogoUrl { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }

        [JsonProperty("pattern")]
        public string Pattern { get; set; }

        [JsonProperty("isNew")]
        public bool IsNew { get; set; }

        [JsonProperty("isPopular")]
        public bool IsPopular { get; set; }
    }
}
