﻿using Newtonsoft.Json;
using System;

namespace Docitt.FileThis
{
    public class FilethisAccountResponse : IFilethisAccountResponse
    {
        [JsonProperty("emailFailure")]
        public bool EmailFailure { get; set; }

        [JsonProperty("priorLoginDate")]
        public DateTime PriorLoginDate { get; set; }

        [JsonProperty("addons")]
        public int Addons { get; set; }

        [JsonProperty("destinationId")]
        public int DestinationId { get; set; }

        [JsonProperty("destinationSettings")]
        public string DestinationSettings { get; set; }

        [JsonProperty("destinationState")]
        public string DestinationState { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("tracking")]
        public string Tracking { get; set; }

        [JsonProperty("emailSuccess")]
        public bool EmailSuccess { get; set; }

        [JsonProperty("subscriptionStorageQuota")]
        public int SubscriptionStorageQuota { get; set; }

        [JsonProperty("weeklyUpdateEnabled")]
        public bool WeeklyUpdateEnabled { get; set; }

        [JsonProperty("features")]
        public string Features { get; set; }

        [JsonProperty("lastClientIp")]
        public object AstClientIp { get; set; }

        [JsonProperty("uploadEmail")]
        public string UploadEmail { get; set; }

        [JsonProperty("referralStorageQuota")]
        public int ReferralStorageQuota { get; set; }

        [JsonProperty("showActiveServices")]
        public object ShowActiveServices { get; set; }

        [JsonProperty("dailyUpdateTime")]
        public DateTime DailyUpdateTime { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("brandingLogoUrl")]
        public string BrandingLogoUrl { get; set; }

        [JsonProperty("plan")]
        public string Plan { get; set; }

        [JsonProperty("totalSourceConnectionQuota")]
        public int TotalSourceConnectionQuota { get; set; }

        [JsonProperty("level")]
        public string Level { get; set; }

        [JsonProperty("partnerAccountId")]
        public string PartnerAccountId { get; set; }

        [JsonProperty("uploadUsername")]
        public string UploadUsername { get; set; }

        [JsonProperty("priorClientIp")]
        public object PriorClientIp { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("emailCampaign")]
        public bool EmailCampaign { get; set; }

        [JsonProperty("dailyUpdate")]
        public bool DailyUpdate { get; set; }

        [JsonProperty("totalSourceConnectionUsage")]
        public int TotalSourceConnectionUsage { get; set; }

        [JsonProperty("socialMediaUrl")]
        public string SocialMediaUrl { get; set; }

        [JsonProperty("subscriptionSourceConnectionQuota")]
        public int SubscriptionSourceConnectionQuota { get; set; }

        [JsonProperty("pendingPlan")]
        public string PendingPlan { get; set; }

        [JsonProperty("autoClassify")]
        public bool AutoClassify { get; set; }

        [JsonProperty("autoMetadataTag")]
        public bool AutoMetadataTag { get; set; }

        [JsonProperty("pendingPlanSourceConnectionQuota")]
        public int PendingPlanSourceConnectionQuota { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("referralSourceConnectionQuota")]
        public int ReferralSourceConnectionQuota { get; set; }

        [JsonProperty("expires")]
        public DateTime Expires { get; set; }

        [JsonProperty("pendingAddons")]
        public int PendingAddons { get; set; }

        [JsonProperty("proServiceId")]
        public int ProServiceId { get; set; }

        [JsonProperty("addOnSourceConnectionQuota")]
        public int AddOnSourceConnectionQuota { get; set; }

        [JsonProperty("eventReminderDays")]
        public int EventReminderDays { get; set; }

        [JsonProperty("lastLoginDate")]
        public DateTime LastLoginDate { get; set; }

        [JsonProperty("renews")]
        public bool Renews { get; set; }

        [JsonProperty("timeoutMinutes")]
        public int TimeoutMinutes { get; set; }

        [JsonProperty("useTimeout")]
        public bool UseTimeout { get; set; }

        [JsonProperty("totalStorageQuota")]
        public int TotalStorageQuota { get; set; }

        [JsonProperty("brandingName")]
        public string BrandingName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("destinationBrowseable")]
        public bool DestinationBrowseable { get; set; }

        [JsonProperty("pendingPlanStorageQuota")]
        public int PendingPlanStorageQuota { get; set; }

        [JsonProperty("addOnStorageQuota")]
        public int AddOnStorageQuota { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("destinationError")]
        public string DestinationError { get; set; }

        [JsonProperty("lastActivity")]
        public DateTime LastActivity { get; set; }

        [JsonProperty("totalStorageUsage")]
        public int TotalStorageUsage { get; set; }

        [JsonProperty("partnerId")]
        public string PartnerId { get; set; }

        [JsonProperty("eventReminder")]
        public bool EventReminder { get; set; }

        [JsonProperty("paymentDate")]
        public DateTime PaymentDate { get; set; }
    }
}
