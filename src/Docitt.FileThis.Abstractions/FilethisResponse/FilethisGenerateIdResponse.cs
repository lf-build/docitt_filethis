﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisGenerateIdResponse : IFilethisGenerateIdResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
