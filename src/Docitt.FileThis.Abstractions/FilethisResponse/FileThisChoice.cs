using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FileThisChoice
    {
        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}