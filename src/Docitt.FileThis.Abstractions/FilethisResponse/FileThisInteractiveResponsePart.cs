using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FileThisInteractiveResponsePart
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}