using System.Collections.Generic;
using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FileThisPart
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("sensitive")]
        public bool Sensitive { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("choices")]
        public IList<FileThisChoice> Choices { get; set; }
    }
}