﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisTokenResponse : IFilethisTokenResponse
    {
        [JsonProperty("expiresIn")]
        public string ExpiresIn { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
