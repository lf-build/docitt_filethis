﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.FileThis
{
    public class FilethisInteractionResponse : IFilethisInteractionResponse
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("parts")]
        public List<FileThisPart> Parts { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("legacyCode")]
        public string LegacyCode { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }
}


