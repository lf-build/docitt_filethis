﻿using Newtonsoft.Json;
using System;

namespace Docitt.FileThis
{
    public class FilethisDocumentResponse : IFilethisDocumentResponse
    {
        [JsonProperty("shared")]
        public bool Shared { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("addedDate")]
        public DateTime AddedDate { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("mimeType")]
        public string MimeType { get; set; }

        [JsonProperty("destinationId")]
        public string DestinationId { get; set; }

        [JsonProperty("deliveredDate")]
        public DateTime DeliveredDate { get; set; }

        [JsonProperty("originalName")]
        public string OriginalName { get; set; }

        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("deliveryState")]
        public string DeliveryState { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("connectionId")]
        public string ConnectionId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("relevantDate")]
        public DateTime RelevantDate { get; set; }

        [JsonProperty("actionDate")]
        public DateTime ActionDate { get; set; }

        [JsonProperty("originalRelevantDate")]
        public DateTime OriginalRelevantDate { get; set; }
    }
}
