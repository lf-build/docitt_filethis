﻿using Newtonsoft.Json;
using System;

namespace Docitt.FileThis
{
    public class FilethisConnectionResponse : IFilethisConnectionResponse
    {
        [JsonProperty("sourceId")]
        public string SourceId { get; set; }

        [JsonProperty("documentCount")]
        public int DocumentCount { get; set; }

        [JsonProperty("tries")]
        public int Tries { get; set; }

        [JsonProperty("period")]
        public string Period { get; set; }

        [JsonProperty("checkedDate")]
        public DateTime CheckedDate { get; set; }

        [JsonProperty("fetchAll")]
        public bool FetchAll { get; set; }

        [JsonProperty("attemptDate")]
        public DateTime AttemptDate { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("successDate")]
        public DateTime SuccessDate { get; set; }

        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("historicalPeriod")]
        public string HistoricalPeriod { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("kickoffDate")]
        public DateTime KickoffDate { get; set; }

        [JsonProperty("validation")]
        public string Validation { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }
}
