﻿namespace Docitt.FileThis
{
    /// <summary>
    /// ConnectionState
    /// </summary>
    public enum ConnectionState
    {
        Created = 0,
        Waiting = 1,
        Connecting = 2,
        Completed = 3,
        Uploading = 4,
        Question = 4,
        Answered = 5,
        Manual = 6,
        Incorrect = 7,
        Error = 8
    }
}
