﻿namespace Docitt.FileThis
{
    /// <summary>
    /// SourceType
    /// </summary>
    public enum SourceType
    {
        None = 0,
        TaxReturns = 1,
        PaystubAndW2s = 2,
        Banking = 3
    }
}
