﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisRequestPartnerUpdate : IFilethisRequestPartnerUpdate
    {
        public string apiKey { get; set; }
        public string apiSecret { get; set; }
        public string cors { get; set; }
        public string deliveryUrl { get; set; }
        public string domainName { get; set; }
        public int historicalPeriod { get; set; }
        public int id { get; set; }
        public bool metadataOnly { get; set; }
        public string metadataVersion { get; set; }
        public string name { get; set; }
        public string s3AwsBucket { get; set; }
        public string s3AwsKey { get; set; }
        public string s3AwsSecret { get; set; }
        public string[] s3CredentialsBinary { get; set; }
        public string userInteractionVersion { get; set; }
    }
}
