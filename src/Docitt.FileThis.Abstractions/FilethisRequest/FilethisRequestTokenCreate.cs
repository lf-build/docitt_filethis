﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisRequestTokenCreate : IFilethisRequestTokenCreate
    {
        [JsonProperty("expiresIn")]
        public int ExpiresIn { get; set; }
    }
}
