﻿using Newtonsoft.Json;
using System;

namespace Docitt.FileThis
{
    public class FilethisRequestConnectionUpdate : IFilethisRequestConnectionUpdate
    {
            public int accountId { get; set; }
            public DateTime attemptDate { get; set; }
            public DateTime checkedDate { get; set; }
            public DateTime createdDate { get; set; }
            public int documentCount { get; set; }
            public bool fetchAll { get; set; }
            public int historicalPeriod { get; set; }
            public int id { get; set; }
            public string info { get; set; }
            public DateTime kickoffDate { get; set; }
            public bool metadataOnly { get; set; }
            public string name { get; set; }
            public string password { get; set; }
            public string period { get; set; }
            public int sourceId { get; set; }
            public string state { get; set; }
            public DateTime successDate { get; set; }
            public int tries { get; set; }
            public string username { get; set; }
            public string validation { get; set; }
    }
}
