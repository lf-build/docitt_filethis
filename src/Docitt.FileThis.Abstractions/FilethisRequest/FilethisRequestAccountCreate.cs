﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisRequestAccountCreate : IFilethisRequestAccountCreate
    {
        [JsonProperty("partnerAccountId")]
        public string PartnerAccountId { get; set; }

        [JsonProperty("historicalPeriod")]
        public int HistoricalPeriod { get; set; }
    }
}
