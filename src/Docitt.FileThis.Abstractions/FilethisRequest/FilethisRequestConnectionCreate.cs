﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class FilethisRequestConnectionCreate : IFilethisRequestConnectionCreate
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("sourceId")]
        public long SourceId { get; set; }

        [JsonProperty("historicalPeriod")]
        public int HistoricalPeriod { get; set; }
    }
}
