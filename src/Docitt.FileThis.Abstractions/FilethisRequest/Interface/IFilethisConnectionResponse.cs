﻿using System;

namespace Docitt.FileThis
{
    public interface IFilethisConnectionResponse
    {
        string SourceId { get; set; }

        int DocumentCount { get; set; }

        int Tries { get; set; }

        string Period { get; set; }

        DateTime CheckedDate { get; set; }

        bool FetchAll { get; set; }

        DateTime AttemptDate { get; set; }

        bool Enabled { get; set; }

        DateTime SuccessDate { get; set; }

        string AccountId { get; set; }

        DateTime CreatedDate { get; set; }

        string Name { get; set; }

        string HistoricalPeriod { get; set; }

        string Id { get; set; }

        string State { get; set; }

        DateTime KickoffDate { get; set; }

        string Validation { get; set; }

        string Info { get; set; }

        string Username { get; set; }
    }
}
