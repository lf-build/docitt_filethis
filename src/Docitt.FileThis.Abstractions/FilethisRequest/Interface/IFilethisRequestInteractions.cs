﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.FileThis
{
    public interface IFilethisRequestInteractions
    {
        string version { get; set; }
        string id { get; set; }
        List<FileThisInteractiveResponsePart> parts { get; set; }
    }
}
