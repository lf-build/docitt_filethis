﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public interface IFilethisRequestAccountCreate
    {
         string PartnerAccountId { get; set; }

         int HistoricalPeriod { get; set; }
    }
}
