﻿using System;

namespace Docitt.FileThis
{
    public interface IFilethisAccountResponse
    {
        bool EmailFailure { get; set; }

        DateTime PriorLoginDate { get; set; }

        int Addons { get; set; }

        int DestinationId { get; set; }

        string DestinationSettings { get; set; }

        string DestinationState { get; set; }

        string Type { get; set; }

        string Tracking { get; set; }

        bool EmailSuccess { get; set; }

        int SubscriptionStorageQuota { get; set; }

        bool WeeklyUpdateEnabled { get; set; }

        string Features { get; set; }

        object AstClientIp { get; set; }

        string UploadEmail { get; set; }

        int ReferralStorageQuota { get; set; }

        object ShowActiveServices { get; set; }

        DateTime DailyUpdateTime { get; set; }

        string Id { get; set; }

        string BrandingLogoUrl { get; set; }

        string Plan { get; set; }

        int TotalSourceConnectionQuota { get; set; }

        string Level { get; set; }

        string PartnerAccountId { get; set; }

        string UploadUsername { get; set; }

        object PriorClientIp { get; set; }

        string FirstName { get; set; }

        bool EmailCampaign { get; set; }

        bool DailyUpdate { get; set; }

        int TotalSourceConnectionUsage { get; set; }

        string SocialMediaUrl { get; set; }

        int SubscriptionSourceConnectionQuota { get; set; }

        string PendingPlan { get; set; }

        bool AutoClassify { get; set; }

        bool AutoMetadataTag { get; set; }

        int PendingPlanSourceConnectionQuota { get; set; }

        string LastName { get; set; }

        int ReferralSourceConnectionQuota { get; set; }

        DateTime Expires { get; set; }

        int PendingAddons { get; set; }

        int ProServiceId { get; set; }

        int AddOnSourceConnectionQuota { get; set; }

        int EventReminderDays { get; set; }

        DateTime LastLoginDate { get; set; }

        bool Renews { get; set; }

        int TimeoutMinutes { get; set; }

        bool UseTimeout { get; set; }

        int TotalStorageQuota { get; set; }

        string BrandingName { get; set; }

        string Email { get; set; }

        bool DestinationBrowseable { get; set; }

        int PendingPlanStorageQuota { get; set; }

        int AddOnStorageQuota { get; set; }

        DateTime CreatedDate { get; set; }

        string DestinationError { get; set; }

        DateTime LastActivity { get; set; }

        int TotalStorageUsage { get; set; }

        string PartnerId { get; set; }

        bool EventReminder { get; set; }

        DateTime PaymentDate { get; set; }
    }
}
