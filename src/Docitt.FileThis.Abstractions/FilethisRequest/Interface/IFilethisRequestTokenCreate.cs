﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public interface IFilethisRequestTokenCreate
    {
        int ExpiresIn { get; set; }
    }
}
