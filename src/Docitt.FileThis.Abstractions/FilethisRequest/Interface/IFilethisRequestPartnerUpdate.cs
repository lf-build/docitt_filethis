﻿namespace Docitt.FileThis
{
    public interface IFilethisRequestPartnerUpdate
    {
        string apiKey { get; set; }
        string apiSecret { get; set; }
        string cors { get; set; }
        string deliveryUrl { get; set; }
        string domainName { get; set; }
        int historicalPeriod { get; set; }
        int id { get; set; }
        bool metadataOnly { get; set; }
        string metadataVersion { get; set; }
        string name { get; set; }
        string s3AwsBucket { get; set; }
        string s3AwsKey { get; set; }
        string s3AwsSecret { get; set; }
        string[] s3CredentialsBinary { get; set; }
        string userInteractionVersion { get; set; }
    }
}
