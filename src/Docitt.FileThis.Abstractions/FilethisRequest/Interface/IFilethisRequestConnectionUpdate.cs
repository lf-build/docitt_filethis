﻿using Newtonsoft.Json;
using System;

namespace Docitt.FileThis
{
    public interface IFilethisRequestConnectionUpdate
    {
        int accountId { get; set; }
        DateTime attemptDate { get; set; }
        DateTime checkedDate { get; set; }
        DateTime createdDate { get; set; }
        int documentCount { get; set; }
        bool fetchAll { get; set; }
        int historicalPeriod { get; set; }
        int id { get; set; }
        string info { get; set; }
        DateTime kickoffDate { get; set; }
        bool metadataOnly { get; set; }
        string name { get; set; }
        string password { get; set; }
        string period { get; set; }
        int sourceId { get; set; }
        string state { get; set; }
        DateTime successDate { get; set; }
        int tries { get; set; }
        string username { get; set; }
        string validation { get; set; }
    }
}
