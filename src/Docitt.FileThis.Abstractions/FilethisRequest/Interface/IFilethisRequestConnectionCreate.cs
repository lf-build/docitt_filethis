﻿namespace Docitt.FileThis
{
    public interface IFilethisRequestConnectionCreate
    {
        string Username { get; set; }

        string Password { get; set; }

        long SourceId { get; set; }

        int HistoricalPeriod { get; set; }
    }
}
