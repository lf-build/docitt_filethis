﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.FileThis
{
    public class FilethisRequestInteractions : IFilethisRequestInteractions
    {
        public string version { get; set; }
        public string id { get; set; }
        public List<FileThisInteractiveResponsePart> parts { get; set; }
    }
}
