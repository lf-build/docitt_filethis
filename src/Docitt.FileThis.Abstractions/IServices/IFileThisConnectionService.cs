﻿using System.Threading.Tasks;

namespace Docitt.FileThis
{
    public interface IFileThisConnectionService
    {
        /// <summary>
        /// Get the connection details by connection Id 
        /// </summary>
        /// <param name="connectionId">filethis connection id</param>
        /// <returns>Returns IResponseConnection</returns>
        Task<IAccount> GetConnection(string connectionId);
        Task<IResponseConnection> ConnectionWorkflowAsync(IRequestConnection request);

        Task<bool> InteractionResponseAsync(IRequestInteraction request);

        Task<IFilethisConnectionResponse> ConnectionCheck(string applicantId, string accountId, string connectionId);

        Task<IResponseConnection> CreateInteractionAsync(IRequestInteraction request);

        Task<IAccount> GetApplicantDetail(string applicationNumber, string applicantId);

        Task<string> GetConnectionFetch(string accountId, string connectionId);

        Task<bool> DeleteConnectionAsync(string accountId, string connectionId, string applicationNumber);
    }
}
