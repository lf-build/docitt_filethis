﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    public interface IFileThisDocumentService
    {
        Task<IList<IDocument>> GetDocumentsByConnectionId(string connectionId);

        Task<bool> DeleteDocument(string documentId);

        Task<Tuple<string, Stream>> DownloadDocument(string applicantId, string documentId);

        Task<IList<IDocument>> UpdateDocumentAsync(string connectionId);
    }
}
