﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    public interface IFileThisSourceService
    {
        Task<IList<ISource>> GetSourcesAsync();

        Task<IList<IResponseSource>> SearchSourceAsync(string sourceType, string keyword);

        Task<IResponseSourceDetail> GetSourceByIdAsync(string Id);

        Task<string> CheckPlaidInstitutionIdExist(string plaidInstitutionId);
    }
}
