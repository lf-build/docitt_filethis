﻿using Docitt.FileThis.Abstractions;
using LendFoundry.Foundation.Client;

namespace Docitt.FileThis
{
    public interface IConfiguration : IDependencyConfiguration
    {
        EventMapping[] Events { get; set; }

        string Url { get; set; }

        string Version { get; set; }

        string Key { get; set; }

        string Secret { get; set; }

        int AccountTokenExpiration { get; set; }

        int PartnerId { get; set; }

        HistoricalPeriod HistoricalPeriod { get; set; }
    }
}