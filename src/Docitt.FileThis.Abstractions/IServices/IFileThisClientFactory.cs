﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.FileThis
{
    public interface IFileThisClientFactory
    {
        IFileThisClient Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
