﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.FileThis
{
    public interface IFileThisNotificationServiceFactory
    {
        IFileThisNotificationService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
