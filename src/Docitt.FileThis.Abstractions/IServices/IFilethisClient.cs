﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    public interface IFileThisClient
    {
        #region Connections

        Task<IFilethisGenerateIdResponse> CreateNewConnectionAsync(IRequestConnection requestConnection, string accountId);

        Task<IList<FilethisConnectionResponse>> GetConnectionsAsync(string accountId);

        Task<IFilethisConnectionResponse> GetConnectionAsync(string accountId, string connectionId);

        Task<IFilethisConnectionResponse> UpdateConnectionAsync(string accountId, string connectionId);

        Task<bool> DeleteConnectionAsync(string accountId, string connectionId);

        Task<IFilethisGenerateIdResponse> FetchConnectionAsync(string accountId, string connectionId);

        Task<IList<FilethisConnectionResponse>> GetConnectionsAsync();

        Task<FilethisGenerateIdResponse> CreateNewConnectionAsync();

        Task<IFilethisConnectionResponse> GetConnectionAsync(string connectionId);

        Task<IFilethisConnectionResponse> UpdateConnectionAsync(string connectionId);

        Task<bool> DeleteConnectionAsync(string connectionId);

        Task<bool> FetchConnectionAsync(string connectionId);

        #endregion

        #region Accounts
        Task<IList<FilethisAccountResponse>> GetAccountsAsync();

        Task<FilethisAccountResponse> GetAccountAsync(string accountId);

        Task<IFilethisGenerateIdResponse> CreateNewAccountAsync(string applicantId);

        Task<bool> DeleteAccountAsync(string accountId);

        Task<dynamic> GetChanges(string fileThisToken);

        #endregion

        #region Token

        Task<IFilethisTokenResponse> CreateTokenAsync(string accountId);

        Task<bool> DeleteTokenAsync(string accountId, string tokenId);

        Task<bool> DeleteTokenAsync(string tokenId);

        #endregion

        #region Partners

        Task<IList<FilethisPartnerResponse>> GetPartnersFilethisAsync();

        Task<FilethisPartnerResponse> GetPartnerByPartnerIdAsync(string partnerId);

        Task<bool> UpdatePartnerAsync(string partnerId);
        #endregion

        #region Interactions

        Task<IFilethisInteractionResponse> GetInteractionAsync(string accountId, string connectionId);

        Task<IFilethisInteractionResponse> GetInteractionByInteractionIdAsync(string accountId, string connectionId, string interactionId);

        Task<bool> InteractionResponseAsync(IRequestInteraction requestModel);
        #endregion

        #region Sources

        Task<IList<FilethisSourceResponse>> GetSourcesAsync();

        #endregion

        #region Documents

        Task<IList<FilethisDocumentResponse>> GetDocumentsAsync(string accountId);

        Task<IFilethisDocumentResponse> GetDocumentAsync(string accountId, string documentId);

        Task<bool> DeleteDocumentAsync(string accountId, string documentId);

        Task<IList<FilethisDocumentResponse>> GetDocumentsAsync();

        Task<IFilethisDocumentResponse> GetDocumentAsync(string documentId);

        Task<bool> DeleteDocumentAsync(string documentId);
        #endregion
    }
}
