﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.FileThis
{
    public interface IFileThisPartnerService
    {
        Task<IList<FilethisPartnerResponse>> GetPartnersAsync();

        Task<FilethisPartnerResponse> GetPartnersByIdAsync(string partnerID);
    }
}
