﻿namespace Docitt.FileThis.Abstractions
{
    public interface IFileThisListener
    {
        void Start();
    }
}
