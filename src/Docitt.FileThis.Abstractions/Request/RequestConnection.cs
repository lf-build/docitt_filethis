﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class RequestConnection : IRequestConnection
    {
        [JsonProperty("applicantId")]
        public string ApplicantId { get; set; }

        [JsonProperty("sourceId")]
        public string SourceId { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        public string ApplicationNumber {get; set;}
    }
}
