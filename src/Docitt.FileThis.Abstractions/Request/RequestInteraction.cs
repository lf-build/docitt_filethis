﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.FileThis
{
    public class RequestInteraction : IRequestInteraction
    {
        [JsonProperty("applicantId")]
        public string ApplicantId { get; set; }

        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("connectionId")]
        public string ConnectionId { get; set; }

        [JsonProperty("interactionId")]
        public string InteractionId { get; set; }

        [JsonProperty("sourceId")]
        public string SourceId { get; set; }

         [JsonProperty("filethisToken")]
        public string FileThisToken {get; set;}

        [JsonProperty("inputs")]
        public Dictionary<string,string>  Inputs { get; set; }

        [JsonProperty("applicationNumber")]
        public string ApplicationNumber {get; set;}
    }
}
