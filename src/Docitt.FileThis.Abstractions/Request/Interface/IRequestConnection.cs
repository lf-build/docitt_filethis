﻿namespace Docitt.FileThis
{
    public interface IRequestConnection
    {
        string ApplicantId { get; set; }

        string SourceId { get; set; }

        string Username { get; set; }

        string Password { get; set; }
        string ApplicationNumber {get; set;}
    }
}
