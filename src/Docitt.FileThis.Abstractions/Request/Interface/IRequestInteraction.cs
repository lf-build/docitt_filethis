﻿using System.Collections.Generic;

namespace Docitt.FileThis
{
    public interface IRequestInteraction
    {
        string ApplicantId { get; set; }

        string AccountId { get; set; }

        string ConnectionId { get; set; }

        string InteractionId { get; set; }

        string SourceId { get; set; }

        string FileThisToken {get; set;}

        Dictionary<string, string> Inputs { get; set; }

        string ApplicationNumber {get; set;}
    }
}
