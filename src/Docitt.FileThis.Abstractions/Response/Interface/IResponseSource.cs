﻿namespace Docitt.FileThis
{
    public interface IResponseSource
    {
         string SourceId { get; set; }

         string SourceName { get; set; }

         string Logo { get; set; }
    }
}
