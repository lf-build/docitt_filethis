﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public interface IResponseConnection
    {
        string ApplicantId { get; set; }

        string AccountId { get; set; }

        string ConnectionId { get; set; }

        string InteractionId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IFilethisInteractionResponse, FilethisInteractionResponse>))]
        IFilethisInteractionResponse Mfa { get; set; }

        string SourceId { get; set; }

        string SourceName { get; set; }

        string Logo { get; set; }

         bool IsMfa { get; set; }

         bool IsError { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IResponseError, ResponseError>))]
        IResponseError Error { get; set; }
    }
}
