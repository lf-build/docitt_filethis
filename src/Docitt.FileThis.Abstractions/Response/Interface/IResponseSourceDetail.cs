﻿using System.Collections.Generic;

namespace Docitt.FileThis
{
    public interface IResponseSourceDetail
    {
        IList<FileThisCredential> Credential { get; set; }

        string SourceId { get; set; }

         string SourceName { get; set; }

         string Logo { get; set; }
    }
}
