﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.FileThis
{
    public class ResponseSourceDetail : IResponseSourceDetail
    {
        [JsonProperty("credentials")]
        public IList<FileThisCredential> Credential { get; set; }

        [JsonProperty("institutionId")]
        public string SourceId { get; set; }

        [JsonProperty("name")]
        public string SourceName { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }
    }
}
