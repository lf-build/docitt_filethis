﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class ResponseConnection : IResponseConnection
    {
        [JsonProperty("applicantId")]
        public string ApplicantId { get; set; }

        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("connectionId")]
        public string ConnectionId { get; set; }

        [JsonProperty("interactionId")]
        public string InteractionId { get; set; }

        [JsonProperty("mfa")]
        public IFilethisInteractionResponse Mfa { get; set; }

        [JsonProperty("institutionId")]
        public string SourceId { get; set; }

        [JsonProperty("name")]
        public string SourceName { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("isMfa")]
        public bool IsMfa { get; set; }

        [JsonProperty("isError")]
        public bool IsError { get; set; }

        [JsonProperty("error")]
        public IResponseError Error { get; set; }
    }
}
