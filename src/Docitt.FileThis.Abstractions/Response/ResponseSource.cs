﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public class ResponseSource : IResponseSource
    {
        [JsonProperty("sourceId")]
        public string SourceId { get; set; }

        [JsonProperty("sourceName")]
        public string SourceName { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }
    }
}
