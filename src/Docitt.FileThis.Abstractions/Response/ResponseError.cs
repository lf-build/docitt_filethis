﻿using Newtonsoft.Json;

namespace Docitt.FileThis
{
    public interface IResponseError
    {
        string Status { get; set; }

        string ErrorMessage { get; set; }
    }
    public class ResponseError : IResponseError
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }
}
